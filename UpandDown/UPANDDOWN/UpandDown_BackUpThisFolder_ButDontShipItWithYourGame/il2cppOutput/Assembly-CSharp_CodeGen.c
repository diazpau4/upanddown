﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Ahogados::OnTriggerEnter(UnityEngine.Collider)
extern void Ahogados_OnTriggerEnter_m92E1A0441373E7139D78BFF21DE40ED148D6D23F (void);
// 0x00000002 System.Void Ahogados::.ctor()
extern void Ahogados__ctor_mD3FE7716DEE41C0487BB03297BEE04B3677494B7 (void);
// 0x00000003 System.Void BotonAzul::Start()
extern void BotonAzul_Start_m2D4D7029A019DA358D7BB53A7841D88DC7F767F7 (void);
// 0x00000004 System.Void BotonAzul::OnCollisionEnter(UnityEngine.Collision)
extern void BotonAzul_OnCollisionEnter_m7DC65CE655E15C9F6438B58489B107BFEA4195BB (void);
// 0x00000005 System.Void BotonAzul::.ctor()
extern void BotonAzul__ctor_m572B1C62B1097E4D72F69E2C93E5BB5712949017 (void);
// 0x00000006 System.Void ActivarPor::Start()
extern void ActivarPor_Start_m5C22223A45AF1728A24BB54A7C4676F76FEB8D19 (void);
// 0x00000007 System.Void ActivarPor::OnCollisionEnter(UnityEngine.Collision)
extern void ActivarPor_OnCollisionEnter_m0C754886B8198350455EFB293140CF4662759D5F (void);
// 0x00000008 System.Void ActivarPor::.ctor()
extern void ActivarPor__ctor_m270F9A44576DF0C592AB81E5F5A35A1E93964031 (void);
// 0x00000009 System.Void Green::Start()
extern void Green_Start_mF98228230F906C86436A24EB60AF10C48B55BA37 (void);
// 0x0000000A System.Void Green::OnCollisionEnter(UnityEngine.Collision)
extern void Green_OnCollisionEnter_m3A8C494BCA75938B3A0198BEDDA3C36A954C46E5 (void);
// 0x0000000B System.Void Green::.ctor()
extern void Green__ctor_m00835381DE782F47DE5DBAEA4F80CFD9F78291FD (void);
// 0x0000000C System.Void Yellow::Start()
extern void Yellow_Start_m198862B08031FBA2FA63705DB34497B8B02F0A5B (void);
// 0x0000000D System.Void Yellow::OnCollisionEnter(UnityEngine.Collision)
extern void Yellow_OnCollisionEnter_mA0033E306B64DCA642028DF34DB8CE856B485EAB (void);
// 0x0000000E System.Void Yellow::.ctor()
extern void Yellow__ctor_m0FE3C71BC14263EDEBDD94305A33F7D34D475542 (void);
// 0x0000000F System.Void bluee::Start()
extern void bluee_Start_m9114C7CF85F0BF44D7ACF035EEBFCF7B1C87A5D1 (void);
// 0x00000010 System.Void bluee::OnCollisionEnter(UnityEngine.Collision)
extern void bluee_OnCollisionEnter_mD1997AAB0D5597F9E8B7AF8978530CD0B700327C (void);
// 0x00000011 System.Void bluee::.ctor()
extern void bluee__ctor_m384C1A168A56BA3E56FF2AEBDE1F1ADF6CAEB65E (void);
// 0x00000012 System.Void Pink::Start()
extern void Pink_Start_m44C106B354A14C2447B8072FF6B66C90536C67BD (void);
// 0x00000013 System.Void Pink::OnCollisionEnter(UnityEngine.Collision)
extern void Pink_OnCollisionEnter_mB6ED49ED1716655D67B78FE1DC39F7D5F41050A4 (void);
// 0x00000014 System.Void Pink::.ctor()
extern void Pink__ctor_m3CD82F261C898520E38C105C22F807CE6A8202A7 (void);
// 0x00000015 System.Void Blue::Start()
extern void Blue_Start_mFF5AE3694F3A3A3E86491B1FB6395838EFB73802 (void);
// 0x00000016 System.Void Blue::OnCollisionEnter(UnityEngine.Collision)
extern void Blue_OnCollisionEnter_m0C2F4B8A3971B0EBD352BDCF8FBD5C9393D3A8C4 (void);
// 0x00000017 System.Void Blue::.ctor()
extern void Blue__ctor_mDFBC60FA30AAE0BD527942FE808EF332F68E3352 (void);
// 0x00000018 System.Void boton::Start()
extern void boton_Start_m4FD740A5C49E167D609A5D2B2976DBAB4CE1DE12 (void);
// 0x00000019 System.Void boton::OnCollisionEnter(UnityEngine.Collision)
extern void boton_OnCollisionEnter_m93CA49978E15DE841A423E1F9576614C77F6F8CC (void);
// 0x0000001A System.Void boton::.ctor()
extern void boton__ctor_m5B5518026F57C5BB5123ADB104189C49C7501B31 (void);
// 0x0000001B System.Void botonn::Start()
extern void botonn_Start_m43EBDF7598048D33A2C39A6BF98E995C6523C4B6 (void);
// 0x0000001C System.Void botonn::OnCollisionEnter(UnityEngine.Collision)
extern void botonn_OnCollisionEnter_m08DCA7099BB99EB940C90BA032E66D5B67CE0173 (void);
// 0x0000001D System.Void botonn::.ctor()
extern void botonn__ctor_mAE820DD557A405F487C109BCC562D54D58BE2950 (void);
// 0x0000001E System.Void botonbrown::Start()
extern void botonbrown_Start_m894EA319FBD22D6E7CBF38C430CF3767547E4FA0 (void);
// 0x0000001F System.Void botonbrown::OnCollisionEnter(UnityEngine.Collision)
extern void botonbrown_OnCollisionEnter_m1FBCA1E0ABB56749CEC6BFBEB7253496BBEE9361 (void);
// 0x00000020 System.Void botonbrown::.ctor()
extern void botonbrown__ctor_mA57A0ECA303F5839869CE70D1D20C90892DC88FD (void);
// 0x00000021 System.Void ChangeMaterialColor::OnTriggerEnter(UnityEngine.Collider)
extern void ChangeMaterialColor_OnTriggerEnter_m72BE367AA34955A64300A3731126416074F26050 (void);
// 0x00000022 System.Void ChangeMaterialColor::.ctor()
extern void ChangeMaterialColor__ctor_mA7C41E8B9D59373CD22D20B5B166CF3005B02BEC (void);
// 0x00000023 System.Void ControlCamara::.ctor()
extern void ControlCamara__ctor_m954699EF3390352A462FE175B54EEB6A85882E34 (void);
// 0x00000024 System.Void DesactivateWater::OnTriggerEnter(UnityEngine.Collider)
extern void DesactivateWater_OnTriggerEnter_mEB78F004B8348CB13AAEBF0E702B53F5264931A0 (void);
// 0x00000025 System.Void DesactivateWater::.ctor()
extern void DesactivateWater__ctor_m4E36BED2656EAF4D31850E5F4349638AB20E8F71 (void);
// 0x00000026 System.Void DisparoVioleta::Start()
extern void DisparoVioleta_Start_mC59801011862F215FC5ABC26A738B102D0600DED (void);
// 0x00000027 System.Void DisparoVioleta::Update()
extern void DisparoVioleta_Update_m42A76DA63F6872D2A1BFE8397E3CFFCC404AF212 (void);
// 0x00000028 System.Void DisparoVioleta::.ctor()
extern void DisparoVioleta__ctor_mFC08BB359B83DB0F95A08FD01FD0421EA4B141C5 (void);
// 0x00000029 System.Void Explocion1::Start()
extern void Explocion1_Start_mDFCC3B7578CEFC3895ECA8D13FC8A84BD5152EC5 (void);
// 0x0000002A System.Void Explocion1::Main()
extern void Explocion1_Main_m71D3468C97B21DFD48540F165D9A4649DF019686 (void);
// 0x0000002B System.Void Explocion1::crearcubo(UnityEngine.Vector3)
extern void Explocion1_crearcubo_m3D9D04598A03AE173F0737C86E80282740BB651B (void);
// 0x0000002C System.Void Explocion1::.ctor()
extern void Explocion1__ctor_mA6B2EB72BBB5B7342EE1FCB7E44772DBB83D6323 (void);
// 0x0000002D System.Void Plataforma::Start()
extern void Plataforma_Start_m5C2F13758A64BD7C311D4FB97F05F2018A0D21BD (void);
// 0x0000002E System.Void Plataforma::OnCollisionEnter(UnityEngine.Collision)
extern void Plataforma_OnCollisionEnter_m0CEFDF54C468EBE694B6E8F6EE952AEA782E3530 (void);
// 0x0000002F System.Void Plataforma::.ctor()
extern void Plataforma__ctor_m0731064900D4E868852A3238F25E9391B71D9DAB (void);
// 0x00000030 System.Void LASER::Start()
extern void LASER_Start_mF867932299EC97FC9BBFB21AE90B44A246A1E37E (void);
// 0x00000031 System.Void LASER::Update()
extern void LASER_Update_m22C0ACEEC6E7236105D0FEA7BC761F7E4E189BF5 (void);
// 0x00000032 System.Collections.IEnumerator LASER::Esperar2()
extern void LASER_Esperar2_m848091CE519478B6FA9637CFD429D60C412AC788 (void);
// 0x00000033 System.Void LASER::.ctor()
extern void LASER__ctor_m7A535AE2240091690D78B0EAD366915082FF0F0F (void);
// 0x00000034 System.Void Crecimiento::Start()
extern void Crecimiento_Start_m26E908274442CE06AA3171F41897E87D227BFD57 (void);
// 0x00000035 System.Collections.IEnumerator Crecimiento::Esperar()
extern void Crecimiento_Esperar_m4FDC9F89F56BD8F06FC1744225659F700B1CA32E (void);
// 0x00000036 System.Void Crecimiento::.ctor()
extern void Crecimiento__ctor_mC85A12B6EC73AB27F3E72D2C9E3F7856253525B9 (void);
// 0x00000037 System.Void Detecta::Start()
extern void Detecta_Start_m9D0CBC13F5F8C37CF26A6BA10AAF2CD4CA8C6672 (void);
// 0x00000038 System.Void Detecta::Update()
extern void Detecta_Update_mE03AA3ED436D0B883DB629970EAD52E5C2A8BC6E (void);
// 0x00000039 System.Collections.IEnumerator Detecta::Esperar()
extern void Detecta_Esperar_m60EB3101E815859B720ED2EF59076A9B5650B3CD (void);
// 0x0000003A System.Void Detecta::.ctor()
extern void Detecta__ctor_mA155C97B7A04E810C4426A360393DB1C081FC2AA (void);
// 0x0000003B System.Void Control::Start()
extern void Control_Start_m471D7E4645930F8FBA139A65636AD195FFA537C0 (void);
// 0x0000003C System.Void Control::Update()
extern void Control_Update_mC0043627EBF0478227B4FA58974F4A1308F26B7E (void);
// 0x0000003D System.Void Control::OnCollisionEnter(UnityEngine.Collision)
extern void Control_OnCollisionEnter_m2EB88A721B7D9ECA37F04E372F8C3E5C532B1D2E (void);
// 0x0000003E System.Void Control::.ctor()
extern void Control__ctor_mD9E42706311F50254A186C325E48A355ADE1FFEB (void);
// 0x0000003F System.Void Explocion2::Start()
extern void Explocion2_Start_mDA5052067A9A668974248262F3C33447AE5EEF9A (void);
// 0x00000040 System.Void Explocion2::Main()
extern void Explocion2_Main_m5359A8A6D940D21BBB652759ED060F5268AAD199 (void);
// 0x00000041 System.Void Explocion2::crearcubo(UnityEngine.Vector3)
extern void Explocion2_crearcubo_m48BD09B4009A560C81A50FFA737313875FE6EE02 (void);
// 0x00000042 System.Void Explocion2::.ctor()
extern void Explocion2__ctor_mBCBD67791CA8C8BDB8C2182DA5B78F866F89AE32 (void);
// 0x00000043 System.Void Disparar::Start()
extern void Disparar_Start_m727BA1F0BEED0F432A750F29E06C9718FBC1EE1B (void);
// 0x00000044 System.Void Disparar::Update()
extern void Disparar_Update_m4B12877AE93E05207B9664473DA29C6C24992E3E (void);
// 0x00000045 System.Collections.IEnumerator Disparar::Esperar2()
extern void Disparar_Esperar2_mDC9FC16456811777C5D645B9DCC6CE3814A12794 (void);
// 0x00000046 System.Collections.IEnumerator Disparar::Esperar()
extern void Disparar_Esperar_m3375FA8E2B9457F2DDA2E7B2BB51EA5A3B29B261 (void);
// 0x00000047 System.Void Disparar::.ctor()
extern void Disparar__ctor_mD25E1D3ABEFCDCAF4CED7C35156666CD7B2B64EF (void);
// 0x00000048 System.Void seguirjugador::Start()
extern void seguirjugador_Start_m81766263DA3ED4933AB1140FA50057C1587355E2 (void);
// 0x00000049 System.Void seguirjugador::Update()
extern void seguirjugador_Update_m452BEEB3D32D3D9142DA7D17F196910E55EF9BE9 (void);
// 0x0000004A System.Void seguirjugador::.ctor()
extern void seguirjugador__ctor_mFB9C0085235BC22AF30AC10CCF53FC9EB9B4473E (void);
// 0x0000004B System.Void Tp::Start()
extern void Tp_Start_mA1B091B30AD514AF13CF22B23CD8145999594751 (void);
// 0x0000004C System.Void Tp::Update()
extern void Tp_Update_m29441DD821897E7DF8E9C9A79F05398CF31D9417 (void);
// 0x0000004D System.Void Tp::logica()
extern void Tp_logica_m0457CDE3CDAAC70EA08DA71E328CD79E581D059B (void);
// 0x0000004E System.Collections.IEnumerator Tp::CTP()
extern void Tp_CTP_m0C60E622B9087CB375BDB17E3424BBA009B737D1 (void);
// 0x0000004F System.Collections.IEnumerator Tp::Esperar()
extern void Tp_Esperar_m3E62D720E0A27EE67E2305EE2A55288BD3F68FBF (void);
// 0x00000050 System.Void Tp::.ctor()
extern void Tp__ctor_mA5872DFE2DD46B7BEE3E4F0EBEA6B8CB528770BA (void);
// 0x00000051 System.Void SeguirPJ::Start()
extern void SeguirPJ_Start_mA115A41773012E74514757EDB4A0F7FD679674C7 (void);
// 0x00000052 System.Void SeguirPJ::Update()
extern void SeguirPJ_Update_m6F489BB363687DA0E8F9B4BDB7FA111C6B415529 (void);
// 0x00000053 System.Void SeguirPJ::OnTriggerEnter(UnityEngine.Collider)
extern void SeguirPJ_OnTriggerEnter_m4063E3877DC8266A941ED5973B87D9E9E6A02F70 (void);
// 0x00000054 System.Collections.IEnumerator SeguirPJ::esperar()
extern void SeguirPJ_esperar_m4C44297E9A9A5B7AB9C3927153D587A8DE69BAB9 (void);
// 0x00000055 System.Void SeguirPJ::.ctor()
extern void SeguirPJ__ctor_m9D7D2B70670B5214395C765429303EE95933224D (void);
// 0x00000056 System.Void Explocion::Start()
extern void Explocion_Start_m1408D753813A0AA0715BD911BB184D4C86E181F7 (void);
// 0x00000057 System.Void Explocion::Main()
extern void Explocion_Main_m7CEE1A7AC344B65E6A5B4DD240E19F14EDA6282D (void);
// 0x00000058 System.Void Explocion::crearcubo(UnityEngine.Vector3)
extern void Explocion_crearcubo_m6869D2FE6115BF2E420765F62A485EC1BC208655 (void);
// 0x00000059 System.Void Explocion::.ctor()
extern void Explocion__ctor_mD0F385BDB7C39E25FD4010028152F3ECEB641F7A (void);
// 0x0000005A System.Void GraplingGun::Awake()
extern void GraplingGun_Awake_m2A1B402D8783947AD7CC81AB631A60BD54BF4D54 (void);
// 0x0000005B System.Void GraplingGun::Update()
extern void GraplingGun_Update_m5A65E3F67F696D16B8E6AA301ADDDAFCE2FB23A6 (void);
// 0x0000005C System.Void GraplingGun::LateUpdate()
extern void GraplingGun_LateUpdate_m0D71BB9C5885827C4F50826E7A61B17DFD1B3A94 (void);
// 0x0000005D System.Void GraplingGun::StartGrapple()
extern void GraplingGun_StartGrapple_m4804421378CA74FDED27B71EEDFF8177BA948540 (void);
// 0x0000005E System.Void GraplingGun::DrawRope()
extern void GraplingGun_DrawRope_mFF37EA0CA9159763A04F0515F56665D6B11D1CB1 (void);
// 0x0000005F System.Void GraplingGun::StopGrapple()
extern void GraplingGun_StopGrapple_mAEF998B55B590678492C22910D2DCD95B8AB49EA (void);
// 0x00000060 System.Void GraplingGun::.ctor()
extern void GraplingGun__ctor_m2589EA13EF8C258FF935CBE9F19BC5357197F5C0 (void);
// 0x00000061 System.Void MoverPlayer::Start()
extern void MoverPlayer_Start_m8D3684BA62CE1B8A2986E314FEE55A805E2FEC08 (void);
// 0x00000062 System.Void MoverPlayer::Update()
extern void MoverPlayer_Update_m06DE8FC334EFBEA2F0DA3893664D1DABC73F2E71 (void);
// 0x00000063 System.Void MoverPlayer::OnCollisionEnter(UnityEngine.Collision)
extern void MoverPlayer_OnCollisionEnter_m4BCCB3348FA2F190D289F2C6C927848FF7A1507E (void);
// 0x00000064 System.Collections.IEnumerator MoverPlayer::Esperar()
extern void MoverPlayer_Esperar_m79A587D4FF47E1BC9B9954FCFE0A2121160DAD87 (void);
// 0x00000065 System.Collections.IEnumerator MoverPlayer::Esperar2()
extern void MoverPlayer_Esperar2_mBA0F26B2A1CB5B1C0F3D5ABB5D455D1FC7AD413C (void);
// 0x00000066 System.Void MoverPlayer::.ctor()
extern void MoverPlayer__ctor_mA450D7E979FB85DAEECFE3D675596EFD97E07A0E (void);
// 0x00000067 System.Void ParedRompe::OnCollisionEnter(UnityEngine.Collision)
extern void ParedRompe_OnCollisionEnter_m88CC385C99AFA3D653EED69A58D05FA7D3FE070F (void);
// 0x00000068 System.Void ParedRompe::.ctor()
extern void ParedRompe__ctor_m48AC4E1CBE378DD7453D29CFC4253E0054250758 (void);
// 0x00000069 System.Void PlatformUpDown::Awake()
extern void PlatformUpDown_Awake_m646621BD90F4BA843E77C0F27DF7EA0747C35418 (void);
// 0x0000006A System.Void PlatformUpDown::Start()
extern void PlatformUpDown_Start_m838577C75B95297CBC35B21EA3A1D84AA1CA8896 (void);
// 0x0000006B System.Void PlatformUpDown::Update()
extern void PlatformUpDown_Update_mFF687F764172AA6321F98F3BDFAA9BBD65705F48 (void);
// 0x0000006C System.Void PlatformUpDown::OnDrawGizmos()
extern void PlatformUpDown_OnDrawGizmos_m4EFE80E284A111C9F48E5F9B69B6179211D0E3C6 (void);
// 0x0000006D System.Void PlatformUpDown::.ctor()
extern void PlatformUpDown__ctor_m513798FEC40D8E965179251A721C30E75619902C (void);
// 0x0000006E System.Void PlayerController::Start()
extern void PlayerController_Start_m37E4A9612CC78B422FBFC15D257767AE6F969D78 (void);
// 0x0000006F System.Void PlayerController::Update()
extern void PlayerController_Update_mE64911CD78BC318BCCB0EDB3D9DF3BC1559C70FA (void);
// 0x00000070 System.Boolean PlayerController::EstaEnPiso()
extern void PlayerController_EstaEnPiso_m7AF1F86892D2A4928A322E029F2B254D998E0386 (void);
// 0x00000071 System.Void PlayerController::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerController_OnCollisionEnter_mC8B062742ECE79680260B372811F8997C4502945 (void);
// 0x00000072 System.Collections.IEnumerator PlayerController::Esperar()
extern void PlayerController_Esperar_mB5ED640C7329FED5D4A3C07E9669EB8317447068 (void);
// 0x00000073 System.Collections.IEnumerator PlayerController::Esperar2()
extern void PlayerController_Esperar2_m99FD955D4865C6D0ED2BEBFDB5ADF40F3BAE64CB (void);
// 0x00000074 System.Void PlayerController::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerController_OnTriggerEnter_m1310BAC5FA7FAE003E04BFDB6173885B41D826E7 (void);
// 0x00000075 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF66473FEFF2DFEB0325A4D9C917C27C6CE4B22F7 (void);
// 0x00000076 System.Void Reinicio::Start()
extern void Reinicio_Start_m117003C8C17E0C7DCEA1A8B7013FAA76887B5103 (void);
// 0x00000077 System.Void Reinicio::Update()
extern void Reinicio_Update_m5904E60C7BD1E64EBA58E66B2650D0CD4346A6FF (void);
// 0x00000078 System.Void Reinicio::OnCollisionEnter(UnityEngine.Collision)
extern void Reinicio_OnCollisionEnter_m74F6394ACF451651F577BF3958E9588454A8106E (void);
// 0x00000079 System.Void Reinicio::.ctor()
extern void Reinicio__ctor_m58C0897293235E3D1EBBC98729F4E3C64204E2EE (void);
// 0x0000007A System.Void RotadorVert::Update()
extern void RotadorVert_Update_m9B9D6D1E0C7B30C7C4C70DFC2AD9188030D8C79A (void);
// 0x0000007B System.Void RotadorVert::.ctor()
extern void RotadorVert__ctor_m87D89EFA72E5B78CB0DD221903719ED30517C33C (void);
// 0x0000007C System.Void Rotator::Update()
extern void Rotator_Update_m5EEA78DA43EE3059DD3A2B5E0A378709E0DF21FA (void);
// 0x0000007D System.Collections.IEnumerator Rotator::Rotate(System.Single)
extern void Rotator_Rotate_m005E1B20EB4BFD7E784152E464D0AF3C6E93E526 (void);
// 0x0000007E System.Void Rotator::.ctor()
extern void Rotator__ctor_m6FA12381051396D6275FE1B56FBD254201D5B127 (void);
// 0x0000007F System.Void SaltoSticky::Start()
extern void SaltoSticky_Start_mD6E26E22872CE0CCB36E46CA0ED3899269E5A516 (void);
// 0x00000080 System.Void SaltoSticky::Update()
extern void SaltoSticky_Update_mF74AF0F24816874518F7D822D3F7911FE443D4BC (void);
// 0x00000081 System.Collections.IEnumerator SaltoSticky::Esperar()
extern void SaltoSticky_Esperar_mD6D079D9EA09532EC93C96B5764148E669027A31 (void);
// 0x00000082 System.Void SaltoSticky::OnCollisionEnter(UnityEngine.Collision)
extern void SaltoSticky_OnCollisionEnter_m6CAA642B0BF11AE72C60FA0F5690F33999715FC6 (void);
// 0x00000083 System.Void SaltoSticky::.ctor()
extern void SaltoSticky__ctor_m722CAF08619C4A7C903967D25C2BDAFE735C1984 (void);
// 0x00000084 System.Void Sticky::Start()
extern void Sticky_Start_m456A9F1487622BFC2DE779A6A0B6CA7FE862FB96 (void);
// 0x00000085 System.Void Sticky::OnCollisionEnter(UnityEngine.Collision)
extern void Sticky_OnCollisionEnter_m9284543F1145ECF54E9A88BB4984B6B7541CD94A (void);
// 0x00000086 System.Void Sticky::.ctor()
extern void Sticky__ctor_m42CB9AC8C971ADFB195FA1E7CFD1AF96A262F86F (void);
// 0x00000087 System.Void Teleporter::OnTriggerEnter(UnityEngine.Collider)
extern void Teleporter_OnTriggerEnter_m5C18DC16B0D477649D35CB1D121CAA11BD842B52 (void);
// 0x00000088 System.Void Teleporter::.ctor()
extern void Teleporter__ctor_m5AC57ECFE09407D61BD1454D78EA0861002D1B86 (void);
// 0x00000089 System.Void pjgrav::Start()
extern void pjgrav_Start_m0B1A369C745645EFA180E19BDE2EA696837399B7 (void);
// 0x0000008A System.Void pjgrav::Update()
extern void pjgrav_Update_m9A16C9D3BC4B84452EA6661798F86DF8D503DB55 (void);
// 0x0000008B System.Void pjgrav::OnTriggerEnter(UnityEngine.Collider)
extern void pjgrav_OnTriggerEnter_mD31E63D2100AC96F0228810518BB386455BA0335 (void);
// 0x0000008C System.Void pjgrav::.ctor()
extern void pjgrav__ctor_mEC3D411B7FD06A9A446E10C401962E705DA376A1 (void);
// 0x0000008D System.Void pjpare::Start()
extern void pjpare_Start_mF1DDD04C107F9EACEF2F922F50C6D7069AB7CB2C (void);
// 0x0000008E System.Void pjpare::Update()
extern void pjpare_Update_mD7F21C2BF6C114C2CC1AFE5BAB899C668EF6C90F (void);
// 0x0000008F System.Void pjpare::ponerpare()
extern void pjpare_ponerpare_m9A6DABBB0AF89FB9387FC430262A4590A6409A6B (void);
// 0x00000090 System.Void pjpare::OnTriggerEnter(UnityEngine.Collider)
extern void pjpare_OnTriggerEnter_m1769393C3448816C7E70DCBD1371FB4CBDF5DA7B (void);
// 0x00000091 System.Void pjpare::.ctor()
extern void pjpare__ctor_mCF0461F6FB0158F03E8B5C4B7A1C3CCB40C05AC9 (void);
// 0x00000092 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mD13A02B63932BDA275E1A788FD72D86D69B9E440 (void);
// 0x00000093 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mD31D1ED1B2C3C82986BFBD18FA584D45167431F3 (void);
// 0x00000094 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m01CC3E959ACECC222DFD8541231EC1E00C024194 (void);
// 0x00000095 System.Void ChatController::.ctor()
extern void ChatController__ctor_mF4343BA56301C6825EB0A71EBF9600525B437BCD (void);
// 0x00000096 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m31A59FA78B78A6457BB20EC10C08C82E4C669140 (void);
// 0x00000097 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_mA06B4508E4574A97B9B0A20B0273557F5AD068D7 (void);
// 0x00000098 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mAB3C67FA11192EFB31545F42D3D8AAB2A662FEB2 (void);
// 0x00000099 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_m21098EFD0904DFD43A3CB2FF536E83F1593C2412 (void);
// 0x0000009A System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mBA9215F94AB29FBA4D3335AEA6FAB50567509E74 (void);
// 0x0000009B System.Void Readme::.ctor()
extern void Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C (void);
// 0x0000009C System.Void laser::OnTriggerEnter(UnityEngine.Collider)
extern void laser_OnTriggerEnter_m40C7D3A94A0C6E0797A5FC87DB26928C0AD0A1FC (void);
// 0x0000009D System.Void laser::.ctor()
extern void laser__ctor_m56644DC99EE860E092DCF6CF0A0A06E728F384F9 (void);
// 0x0000009E System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_mD139A23C440A026E47FA8E3AD01AD6FEF7713C3D (void);
// 0x0000009F System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_mF6477F5EB75EC15CD6B81ACD85271F854BABC5D6 (void);
// 0x000000A0 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mCA5EA200223A9F224F2F4DBD306DAE038C71A35F (void);
// 0x000000A1 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mBB38130850945A40631821275F07C19720E0C55E (void);
// 0x000000A2 TMPro.TMP_TextEventHandler_CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mF70DBE3FF43B3D6E64053D37A2FADF802533E1FF (void);
// 0x000000A3 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler_CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m237C99FE66E4E16518DAE68FF9CBF1A52E816AD2 (void);
// 0x000000A4 TMPro.TMP_TextEventHandler_SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m395603314F8CD073897DCAB5513270C6ADD94BF4 (void);
// 0x000000A5 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler_SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_mAAE4B440E34EE5736D43D6A8A7D3A7CEE0503D69 (void);
// 0x000000A6 TMPro.TMP_TextEventHandler_WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_m415F4479934B1739658356B47DF4C2E90496AE2E (void);
// 0x000000A7 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler_WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m6062C0AF2FDD8752DC4A75663EE8E5C128504698 (void);
// 0x000000A8 TMPro.TMP_TextEventHandler_LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_m8E724700CC5DF1197B103F87156576A52F62AB2B (void);
// 0x000000A9 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler_LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m1A8E37D2069EF684EF930D4F1ABE764AE17D9A62 (void);
// 0x000000AA TMPro.TMP_TextEventHandler_LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m221527467F0606DD3561E0FB0D7678AA8329AD5D (void);
// 0x000000AB System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler_LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m1376CC9B70177B0C25ACEDF91D5B94BC4B8CF71D (void);
// 0x000000AC System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m9A353CC9705A9E824A60C3D2D026A7FD96B41D74 (void);
// 0x000000AD System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_m2F3241223A91F9C50E11B27F67BA2B6D19328B72 (void);
// 0x000000AE System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m1827A9D3F08839023DE71352202FE5F744E150EF (void);
// 0x000000AF System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_m788B93D2C3B54BCF09475675B274BCB047D449FB (void);
// 0x000000B0 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_mBC44C107A6FB8C43F7C6629D4A15CA85471A28B2 (void);
// 0x000000B1 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_mEF24BCE06B0CE4450B6AE9561EC4B5052DAF00F6 (void);
// 0x000000B2 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m7C4D266070EE2ADC66BCCFD50EB74FEB4923B77E (void);
// 0x000000B3 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mAAF4AF44929D0C9FD73C89E5266028908074AEB1 (void);
// 0x000000B4 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m082D12F7D044456D8514E4D31944C6900F8262C0 (void);
// 0x000000B5 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_mEA56AE9489B50CF5E5FC682AA18D1CE9AF8E1F8B (void);
// 0x000000B6 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mC0055F208B85783F0B3DB942137439C897552571 (void);
// 0x000000B7 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m2FA501D2C572C46A61635E0A3E2FF45EC3A3749C (void);
// 0x000000B8 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m976ED5172DEAFC628DE7C4C51DF25B1373C7846A (void);
// 0x000000B9 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mD92CA5A254960EB149966C2A3243514596C96EAD (void);
// 0x000000BA System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m2D028BFC6EFB4C84C1A7A98B87A509B27E75BA06 (void);
// 0x000000BB System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m7CA524C53D9E88510EE7987E680F49E8353E4B64 (void);
// 0x000000BC System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_m8D1A987C39FD4756642011D01F35BDC3B1F99403 (void);
// 0x000000BD System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m73F65BA012D86A6BE17E82012AE8E2339CA5D550 (void);
// 0x000000BE System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m9A5E67EA64AAC56D56C7D269CC9685E78276360A (void);
// 0x000000BF System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_m22D98FCFC356D5CD7F401DE7EDCDAF7AE0219402 (void);
// 0x000000C0 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_mDAC3E3BE80C9236562EFB6E74DEBE67D8713101D (void);
// 0x000000C1 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m581B79998DB6946746CBF7380AFCC7F2B75D99F7 (void);
// 0x000000C2 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_mA9D72DB0BB6E4F72192DA91BC9F8918A9C61B676 (void);
// 0x000000C3 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_mDC862C8119AB0B4807245CC3482F027842EAB425 (void);
// 0x000000C4 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m198C209AC84EF7A2437ADB2B67F6B78D12AB9216 (void);
// 0x000000C5 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m2108A608ABD8EA7FD2B47EE40C07F4117BB7607E (void);
// 0x000000C6 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mDA26D26457D277CC2D9042F3BD623D48849440C4 (void);
// 0x000000C7 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mC50AAC1AF75B07CD6753EA3224C369E43001791B (void);
// 0x000000C8 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m2832B9D713355ECF861642D115F86AA64A6F119E (void);
// 0x000000C9 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m8E01638EBFE80CC0B9E4A97AB809B91E3C6956BE (void);
// 0x000000CA System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_m24F4FADC328B0C76264DE24663CFA914EA94D1FD (void);
// 0x000000CB System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mC45F318132D23804CBF73EA2445EF7589C2333E9 (void);
// 0x000000CC System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mC3894CE97A12F50FB225CA8F6F05A7B3CA4B0623 (void);
// 0x000000CD System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m22A3AE8E48128DF849EE2957F4EF881A433CA8CB (void);
// 0x000000CE System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_m742F828A2245E8CC29BC045A999C5E527931DFF1 (void);
// 0x000000CF System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mA2284F621031B4D494AC06B687AF43D2D1D89BD7 (void);
// 0x000000D0 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m51C217E0CB26C2E627BA01599147F69B893EF189 (void);
// 0x000000D1 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m6A9CEFA12DB252E297E41E256698DD4E90809F6A (void);
// 0x000000D2 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m3CE7B666BEF4CFFE9EB110C8D57D9A5F6385720B (void);
// 0x000000D3 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m4A69C47EA665D49482B930F924E49C8E70FAC225 (void);
// 0x000000D4 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m11DC90EB1A059F4201457E33C4422A7BDA90F099 (void);
// 0x000000D5 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m9CE8A9F929B99B2318A6F8598EE20E1D4E842ECD (void);
// 0x000000D6 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m8F48CBCC48D4CD26F731BA82ECBAC9DC0392AE0D (void);
// 0x000000D7 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m9F5CE74EDA110F7539B4081CF3EE6B9FCF40D4A7 (void);
// 0x000000D8 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m906CC32CE5FE551DF29928581FFF7DE589C501F2 (void);
// 0x000000D9 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_m614FA9DE53ECB1CF4C6AF6BBC58CE35CA904EB32 (void);
// 0x000000DA System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m16AB65EF6AB38F237F5A6D2D412AB7E5BF7B1349 (void);
// 0x000000DB System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m537A709F25C3AA752437A025BEE741BD2F71320E (void);
// 0x000000DC System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mD86AC3A8D918D14200BF80A354E0E43DC5A565A2 (void);
// 0x000000DD System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_m22D9B03F3E1269B8B104E76DA083ED105029258A (void);
// 0x000000DE System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m42813B343A1FDD155C6BFBFCB514E084FB528DA0 (void);
// 0x000000DF System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mC6992B7B1B6A441DEC5315185E3CE022BB567D61 (void);
// 0x000000E0 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mEC541297C2228C26AB54F825705F0476D45F877A (void);
// 0x000000E1 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_mA1170F805C77CC89B818D8FBEE533846AF66509C (void);
// 0x000000E2 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mB871339347DCB016E019F509A00BDE9A58105822 (void);
// 0x000000E3 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m44A79DDBDF03F254BAFB97BE3E42845B769136C5 (void);
// 0x000000E4 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA67343988C9E9B71C981A9FFAD620C4A9A6AA267 (void);
// 0x000000E5 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m1EA6A5E31F88A1C7E20167A3BCCE427E9E828116 (void);
// 0x000000E6 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m82972EF3AF67EAAFD94A5EE3EA852CE15BE37FC1 (void);
// 0x000000E7 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m40594D716F53E6E5BC0ECD2FFE8ECA44FAA5C8E4 (void);
// 0x000000E8 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m8462C2DC4F71BDE295BE446B213B73F78442E264 (void);
// 0x000000E9 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m3AC7467ECE689A58590DA325F8B300B08C1E1B5D (void);
// 0x000000EA System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m081D44F31AA16E345F914869A07BD47D118707DF (void);
// 0x000000EB System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m217B6E2FC4029A304908EE9DC1E4AA2885CBF8A3 (void);
// 0x000000EC System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m24A7CCA0D93F17AC1A12A340277C706B5C2F9BAB (void);
// 0x000000ED System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m6088452529A70A6684BD8936872B71451779A2F4 (void);
// 0x000000EE System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m79EEE4DF7792F553F5DEDCF0094DAC6F2A58137A (void);
// 0x000000EF System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m745577078D86EF6C23B914BD03EA1A1D169B9B7B (void);
// 0x000000F0 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mF6C09A2C64F5D2619014ADD50039358FAD24DB3E (void);
// 0x000000F1 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_mB0AAA8D034FC575EB3BCF7B0D4514BD110178AD3 (void);
// 0x000000F2 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m13B20506F762769F099DE10B3CCA2DF194192B42 (void);
// 0x000000F3 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mD53FD60E0C5930231FB16BDEA37165FF46D85F6E (void);
// 0x000000F4 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m1D03D0E14D6054D292334C19030256B666ACDA0E (void);
// 0x000000F5 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_m494C501CF565B1ED7C8CB2951EB6FB4F8505637F (void);
// 0x000000F6 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m255A7821E5BA4A7A75B9276E07BC9EA7331B5AA6 (void);
// 0x000000F7 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_mA4A02EB5C853A44F251F43F0AD5967AE914E2B0F (void);
// 0x000000F8 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m04555F8DF147C553FA2D59E33E744901D811B615 (void);
// 0x000000F9 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mDF0FDFBCD11955E0E1D1C9E961B6AD0690C669ED (void);
// 0x000000FA System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m99ACA1D2410917C5837321FC5AC84EAED676D4CC (void);
// 0x000000FB System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m639E300B56757BDB94766447365E1C94B5B83ACD (void);
// 0x000000FC System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay_FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m74B8F0AE15DA6C9968C482981EBCF5CC9DB1F43D (void);
// 0x000000FD System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m570C4B4CB3126622D6DFF71158336313C45C717A (void);
// 0x000000FE System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m5F758974DA88ED8187E71A5100D2D9E47985E359 (void);
// 0x000000FF System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_mC32B726B6202883E12E1A62A52E50092E7E9D9F0 (void);
// 0x00000100 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m6CDFDC88D47FE66021C133974C8CB0E16B08A00E (void);
// 0x00000101 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m4F61F06DFE11CFAF9B064CCA5B2D6423D5CFC302 (void);
// 0x00000102 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m572903C9070A8AD276D2CB14DF7659AE551C75B3 (void);
// 0x00000103 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m1A36B043E4EDD945C93DFC49F7FAFB7034728593 (void);
// 0x00000104 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m6F9BE1975CB15EE559D3B617E8972C8812B41325 (void);
// 0x00000105 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m73C6B3DAA27778B666B9B3B75C9D4641FC1BEC8A (void);
// 0x00000106 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mC24F1D67B99F0AFE7535143BB60C530C8E8735F0 (void);
// 0x00000107 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_m68CAA9BDC1DF3454ECB7C5496A5A2020F84027B8 (void);
// 0x00000108 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4DB9B9E3836D192AA7F42B7EBDC31883E39610E9 (void);
// 0x00000109 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_mD99CC6A70945373DA699327F5A0D0C4516A7D02A (void);
// 0x0000010A System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m3286BB7639F6042AD4437D41BF4633326C4290BE (void);
// 0x0000010B System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m61ABFBAA95ED83A248FBCC3F5823907824434D34 (void);
// 0x0000010C System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m65A61DF0BA640F2787DD87E93B8FD9DEC14AACB4 (void);
// 0x0000010D System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m11DBC535BA8001B48A06F61515C0787C3A86611F (void);
// 0x0000010E System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mB3C405B4856E9B437E13E4BD85DAE73FFF1F6561 (void);
// 0x0000010F System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m436D4CD2A82C0F95B8D942DF4CCFCE09792EDF0F (void);
// 0x00000110 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_mDB693DABF1C3BA92B7A3A4E1460F28E3FAFB444D (void);
// 0x00000111 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_mAEEAA831C084B447DFD0C91A5FA606CB26D3E22A (void);
// 0x00000112 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mF01B64B3E5FE5B648DE2EED031962D9183C2D238 (void);
// 0x00000113 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m9CDB87631C324FB89FA7D52F5BC910146F3DDEB7 (void);
// 0x00000114 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m9B68D69B87E07DC0154344E5276EFC5B11205718 (void);
// 0x00000115 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9699A62E72D3A262EDFDF7AC63ABD33E53F179B6 (void);
// 0x00000116 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m1B592E7AC81C7F17D0A59325EBDE71E067178E8A (void);
// 0x00000117 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_m5567B541D1602AD54B0F437D4710BCD1951FE2C5 (void);
// 0x00000118 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mB698E212B8C3B7C66C71C88F4761A4F33FCE4683 (void);
// 0x00000119 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_m09CFD4E872042E7377BEC8B95D34F22F62ABC45B (void);
// 0x0000011A System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m1CCF60CA14B2FF530950D366FF078281ADC48FF4 (void);
// 0x0000011B System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_mC19C148659C8C97357DB56F36E14914133DA93CF (void);
// 0x0000011C System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_mD2ABEB338822E0DBCFDEC1DB46EC20BB2C44A8C2 (void);
// 0x0000011D System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m60947EACA10408B6EAD2EC7AC77B4E54E2666DD8 (void);
// 0x0000011E System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_mF5AF9069E523C0DF610EF3BE2017E73A4609E3CC (void);
// 0x0000011F System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m96D17C13A279F9B442588F6448672BCF07E4A409 (void);
// 0x00000120 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_mE761EE13D2F9573841EFA5DEE82E545545280BFA (void);
// 0x00000121 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC97FED540BDE59254AB30C5E6BCF1F53B766945F (void);
// 0x00000122 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_mD84B9A0167705D5D3C8CA024D479DC7B5362E67B (void);
// 0x00000123 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mA59F3CA197B3A474F4D795E2B3F182179FF633CF (void);
// 0x00000124 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m8D4DD5CA81E5B0C02937D43C1782533D54F34B3F (void);
// 0x00000125 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m4473873008D4A843A26F0D2353FC36ABE74CEED2 (void);
// 0x00000126 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m24BEF670ABD1CCC864FAFE04750FEB83D916D0DF (void);
// 0x00000127 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mCC864EE9569AF68F53F6EAEB2CE8077CFEAF9E53 (void);
// 0x00000128 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mAF3B23F4DD98AC3E641700B994B2994C14F0E12C (void);
// 0x00000129 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_mDCAEC737A20F161914DD12A2FCBAB6AB7C64FEF2 (void);
// 0x0000012A System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m5F98434E568929859E250743BA214C0782D17F2B (void);
// 0x0000012B System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m01E35B4259BA0B13EC5DA18A11535C2EC6344123 (void);
// 0x0000012C System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mACD450919605863EC4C36DA360898BAEBBF3DDDB (void);
// 0x0000012D System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m6910FF4AC88466E3B7DB867AD553429F1745320D (void);
// 0x0000012E System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mBFB58FE53145750AD747B38D9D1E7E00F8DBF9A0 (void);
// 0x0000012F System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_m328088EC0F893B3E60BB072F77ADF939B8566E4D (void);
// 0x00000130 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_m67833553C2739616BF059C661F92A032885510B2 (void);
// 0x00000131 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m9643904751E2DF0A7DF536847AEA1A2B0774DD20 (void);
// 0x00000132 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_mDF931C271901519BF21FD356F706FA8CDE236406 (void);
// 0x00000133 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m2C738EA265E2B35868110EE1D8FCBD4F1D61C038 (void);
// 0x00000134 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mAAAB1687869E0121C858C65BDB2D294D55CFB67A (void);
// 0x00000135 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mC3EAA1AE81FB3DA4B25F20E557EC6627A06DCB31 (void);
// 0x00000136 System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_mE3D6E47455F101F2DEEBC2A58D09A97CF38E80B8 (void);
// 0x00000137 UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47 (void);
// 0x00000138 System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_mBCD24408A4A2C4053F2F98DB808BD6DE88CA998F (void);
// 0x00000139 System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_m8DE12FC1A6C31D2D60ED78F0B574CE3F864F546E (void);
// 0x0000013A System.Void _3Gear.Core.Engine.Pendulum::Start()
extern void Pendulum_Start_mA7335B73B384BA4338C44C2C9FDB05A03F69B5F2 (void);
// 0x0000013B System.Void _3Gear.Core.Engine.Pendulum::Update()
extern void Pendulum_Update_mDECB7850769049DBFD6AAFE677CBEB5426924853 (void);
// 0x0000013C System.Void _3Gear.Core.Engine.Pendulum::FixedUpdate()
extern void Pendulum_FixedUpdate_m697D099D4FC375A5578E8CADFB2FEF62FE271346 (void);
// 0x0000013D System.Void _3Gear.Core.Engine.Pendulum::ResetTimer()
extern void Pendulum_ResetTimer_m6411AF768C9A79FB1AA0B733FB160F08FB7C9575 (void);
// 0x0000013E UnityEngine.Quaternion _3Gear.Core.Engine.Pendulum::PendulumRotation(System.Single)
extern void Pendulum_PendulumRotation_mECEDBA4718747F410C3CE2F2A216F0B9601A5B13 (void);
// 0x0000013F System.Void _3Gear.Core.Engine.Pendulum::.ctor()
extern void Pendulum__ctor_mD16958C87AFE52A859DEBECDDBE967CE28E6BF1B (void);
// 0x00000140 System.Void LASER_<Esperar2>d__4::.ctor(System.Int32)
extern void U3CEsperar2U3Ed__4__ctor_mEEAD5D9F33D25C65EBBF914C14E6D7CF536C2421 (void);
// 0x00000141 System.Void LASER_<Esperar2>d__4::System.IDisposable.Dispose()
extern void U3CEsperar2U3Ed__4_System_IDisposable_Dispose_mB749B62A36613A4A052C828E3263F1C9F97DB7B7 (void);
// 0x00000142 System.Boolean LASER_<Esperar2>d__4::MoveNext()
extern void U3CEsperar2U3Ed__4_MoveNext_m9B74FD9347E8FBDE5D01648CB46339432E16912E (void);
// 0x00000143 System.Object LASER_<Esperar2>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperar2U3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA7CB69F89CF3836F2385A2AF92BA4CBC5E27428 (void);
// 0x00000144 System.Void LASER_<Esperar2>d__4::System.Collections.IEnumerator.Reset()
extern void U3CEsperar2U3Ed__4_System_Collections_IEnumerator_Reset_mDA16E80695718B03E327E734CCA2E9F8276DD6FE (void);
// 0x00000145 System.Object LASER_<Esperar2>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CEsperar2U3Ed__4_System_Collections_IEnumerator_get_Current_mB6CF901683B5AB6FC94119A8A9E61E6AF61BBC07 (void);
// 0x00000146 System.Void Crecimiento_<Esperar>d__5::.ctor(System.Int32)
extern void U3CEsperarU3Ed__5__ctor_mF98B2CA970F0CDF9F51A438C9DD1CFA78C7D7AB1 (void);
// 0x00000147 System.Void Crecimiento_<Esperar>d__5::System.IDisposable.Dispose()
extern void U3CEsperarU3Ed__5_System_IDisposable_Dispose_m48C0B099D941C0067536458198A22D50747487BB (void);
// 0x00000148 System.Boolean Crecimiento_<Esperar>d__5::MoveNext()
extern void U3CEsperarU3Ed__5_MoveNext_mC2BFFA96A2DB009E9B8E412E0063A4CAC961E90F (void);
// 0x00000149 System.Object Crecimiento_<Esperar>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperarU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA30AD4149C5B681388566CAB5518A32096A967C4 (void);
// 0x0000014A System.Void Crecimiento_<Esperar>d__5::System.Collections.IEnumerator.Reset()
extern void U3CEsperarU3Ed__5_System_Collections_IEnumerator_Reset_m5F283904469E77E19A0989FBF9213E68B3D50A16 (void);
// 0x0000014B System.Object Crecimiento_<Esperar>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CEsperarU3Ed__5_System_Collections_IEnumerator_get_Current_m5FC44F787B3E5E72172CBB728A08C2B9C6C2123A (void);
// 0x0000014C System.Void Detecta_<Esperar>d__9::.ctor(System.Int32)
extern void U3CEsperarU3Ed__9__ctor_mFE96FF71D8CD21E8BC13B57D748376DE3A69A701 (void);
// 0x0000014D System.Void Detecta_<Esperar>d__9::System.IDisposable.Dispose()
extern void U3CEsperarU3Ed__9_System_IDisposable_Dispose_m000643BDFC09ACBACAF93905260830C990EFAFC3 (void);
// 0x0000014E System.Boolean Detecta_<Esperar>d__9::MoveNext()
extern void U3CEsperarU3Ed__9_MoveNext_m35303B850631FE5382C0FB7A276DE1130BE2E485 (void);
// 0x0000014F System.Object Detecta_<Esperar>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperarU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65E995C16007E8ABACEC74F6CADD0F7930C993DE (void);
// 0x00000150 System.Void Detecta_<Esperar>d__9::System.Collections.IEnumerator.Reset()
extern void U3CEsperarU3Ed__9_System_Collections_IEnumerator_Reset_m0772DFCFD8BF68B10CD33C69F45A5B3D87EC4E94 (void);
// 0x00000151 System.Object Detecta_<Esperar>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CEsperarU3Ed__9_System_Collections_IEnumerator_get_Current_m1034C81EE042884DEAEAAC5D6CFF371A2D53BC3B (void);
// 0x00000152 System.Void Disparar_<Esperar2>d__14::.ctor(System.Int32)
extern void U3CEsperar2U3Ed__14__ctor_m19B1D16699B427843E612B157A58BACB8BFE9E21 (void);
// 0x00000153 System.Void Disparar_<Esperar2>d__14::System.IDisposable.Dispose()
extern void U3CEsperar2U3Ed__14_System_IDisposable_Dispose_m0C0D8FF5276064E1192C03A10D3FC852059F602C (void);
// 0x00000154 System.Boolean Disparar_<Esperar2>d__14::MoveNext()
extern void U3CEsperar2U3Ed__14_MoveNext_mAAC40F84DB540E2EA3DDC24EACF7235197388FF0 (void);
// 0x00000155 System.Object Disparar_<Esperar2>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperar2U3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0852DCCF04AD80877FCF0DDE635F3DB2DCCF3429 (void);
// 0x00000156 System.Void Disparar_<Esperar2>d__14::System.Collections.IEnumerator.Reset()
extern void U3CEsperar2U3Ed__14_System_Collections_IEnumerator_Reset_m5A17D74E0F77D43C1C52EB3773958F7B7C44D2E3 (void);
// 0x00000157 System.Object Disparar_<Esperar2>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CEsperar2U3Ed__14_System_Collections_IEnumerator_get_Current_m0AE458DC3830D871B550C70864B2CFCC624F8854 (void);
// 0x00000158 System.Void Disparar_<Esperar>d__15::.ctor(System.Int32)
extern void U3CEsperarU3Ed__15__ctor_m4CCEAB006DBD65D534E09B0582E4CA6FD0179526 (void);
// 0x00000159 System.Void Disparar_<Esperar>d__15::System.IDisposable.Dispose()
extern void U3CEsperarU3Ed__15_System_IDisposable_Dispose_m82C0C5D3752A6E9EB45AB86D4E8E34851D5F618E (void);
// 0x0000015A System.Boolean Disparar_<Esperar>d__15::MoveNext()
extern void U3CEsperarU3Ed__15_MoveNext_mC9CBBD4B30B42635C0C3E0D766957CF573139063 (void);
// 0x0000015B System.Object Disparar_<Esperar>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperarU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m415825A3AD32E7530EA3D6FBE7A1CFDD9640F9FD (void);
// 0x0000015C System.Void Disparar_<Esperar>d__15::System.Collections.IEnumerator.Reset()
extern void U3CEsperarU3Ed__15_System_Collections_IEnumerator_Reset_mCCF4D28828C9B7255621E727D9E1C7CBE7AF42E2 (void);
// 0x0000015D System.Object Disparar_<Esperar>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CEsperarU3Ed__15_System_Collections_IEnumerator_get_Current_mC64653195CAAEAF967B996CA7DB8459B2A5DC314 (void);
// 0x0000015E System.Void Tp_<CTP>d__20::.ctor(System.Int32)
extern void U3CCTPU3Ed__20__ctor_m13FF4687E630CAAD1B72AFA6FEA50BD196A87D66 (void);
// 0x0000015F System.Void Tp_<CTP>d__20::System.IDisposable.Dispose()
extern void U3CCTPU3Ed__20_System_IDisposable_Dispose_m347F3B07EE2A4E9638D2A300947A35BC8928982D (void);
// 0x00000160 System.Boolean Tp_<CTP>d__20::MoveNext()
extern void U3CCTPU3Ed__20_MoveNext_m5150B1278F44FD3FE347EE0214A168EC757360F9 (void);
// 0x00000161 System.Object Tp_<CTP>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCTPU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2D9DC535DD170AC024562E1F0E246D6F4A28E6F (void);
// 0x00000162 System.Void Tp_<CTP>d__20::System.Collections.IEnumerator.Reset()
extern void U3CCTPU3Ed__20_System_Collections_IEnumerator_Reset_m91EBF6C4294874C33CDF41C7C35F4E8BF7E975DD (void);
// 0x00000163 System.Object Tp_<CTP>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CCTPU3Ed__20_System_Collections_IEnumerator_get_Current_m5D3F4E644161CBAEC91211343BE330082786C126 (void);
// 0x00000164 System.Void Tp_<Esperar>d__21::.ctor(System.Int32)
extern void U3CEsperarU3Ed__21__ctor_m3CC245D7EF5D799683F69A88AB457A41B44C084F (void);
// 0x00000165 System.Void Tp_<Esperar>d__21::System.IDisposable.Dispose()
extern void U3CEsperarU3Ed__21_System_IDisposable_Dispose_mF5D320F684B4DEDCC097347244CC2B7088FB8203 (void);
// 0x00000166 System.Boolean Tp_<Esperar>d__21::MoveNext()
extern void U3CEsperarU3Ed__21_MoveNext_mEC518B9252F7B6B939E8C6A906CFC554DC478DB8 (void);
// 0x00000167 System.Object Tp_<Esperar>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperarU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B7E3C850D6509FCE77D1ACEC1C86F562E2876C2 (void);
// 0x00000168 System.Void Tp_<Esperar>d__21::System.Collections.IEnumerator.Reset()
extern void U3CEsperarU3Ed__21_System_Collections_IEnumerator_Reset_m7DDB04513B42D3767954D54CA221AEE29424ACEB (void);
// 0x00000169 System.Object Tp_<Esperar>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CEsperarU3Ed__21_System_Collections_IEnumerator_get_Current_mEC67BE8C0D0A4AC86AB1DAF69173A8C7C838213B (void);
// 0x0000016A System.Void SeguirPJ_<esperar>d__8::.ctor(System.Int32)
extern void U3CesperarU3Ed__8__ctor_m3DC3E4CB3591D026A4AACF6CF16E0DB58732E400 (void);
// 0x0000016B System.Void SeguirPJ_<esperar>d__8::System.IDisposable.Dispose()
extern void U3CesperarU3Ed__8_System_IDisposable_Dispose_m20A8262A50ACD9BF1D5161BC246932BC335B0C60 (void);
// 0x0000016C System.Boolean SeguirPJ_<esperar>d__8::MoveNext()
extern void U3CesperarU3Ed__8_MoveNext_m340C68270030B94F266921FDEAEFE177A8868010 (void);
// 0x0000016D System.Object SeguirPJ_<esperar>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CesperarU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCAB13AF0233F0E6D1B81B33B43379A4BE7FCEB11 (void);
// 0x0000016E System.Void SeguirPJ_<esperar>d__8::System.Collections.IEnumerator.Reset()
extern void U3CesperarU3Ed__8_System_Collections_IEnumerator_Reset_mC34EAE423CA444E087BC0A061CFFEEC224814ADF (void);
// 0x0000016F System.Object SeguirPJ_<esperar>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CesperarU3Ed__8_System_Collections_IEnumerator_get_Current_m26F482F0A7752ADA056A65DE2A76DB52FD7A31FF (void);
// 0x00000170 System.Void MoverPlayer_<Esperar>d__9::.ctor(System.Int32)
extern void U3CEsperarU3Ed__9__ctor_m70E77CBA65B0C3B56C691C237960570442A29C21 (void);
// 0x00000171 System.Void MoverPlayer_<Esperar>d__9::System.IDisposable.Dispose()
extern void U3CEsperarU3Ed__9_System_IDisposable_Dispose_mDAAE9F2DB06DFC387D8E5BF677D617C513F31475 (void);
// 0x00000172 System.Boolean MoverPlayer_<Esperar>d__9::MoveNext()
extern void U3CEsperarU3Ed__9_MoveNext_mBC27437A9808C4BC0551363F311993BC87A25646 (void);
// 0x00000173 System.Object MoverPlayer_<Esperar>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperarU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m133094AC9AA71EA92C1A0665823BE7B90B7751C6 (void);
// 0x00000174 System.Void MoverPlayer_<Esperar>d__9::System.Collections.IEnumerator.Reset()
extern void U3CEsperarU3Ed__9_System_Collections_IEnumerator_Reset_mEF1C53CB325B24E51F1C46070E50544EF0E1C042 (void);
// 0x00000175 System.Object MoverPlayer_<Esperar>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CEsperarU3Ed__9_System_Collections_IEnumerator_get_Current_m98B814A95A6376EAB0277CD8C01D8171A273890F (void);
// 0x00000176 System.Void MoverPlayer_<Esperar2>d__10::.ctor(System.Int32)
extern void U3CEsperar2U3Ed__10__ctor_mA915BCE09F0A20AF11455CF3182A4BE1F28C60A5 (void);
// 0x00000177 System.Void MoverPlayer_<Esperar2>d__10::System.IDisposable.Dispose()
extern void U3CEsperar2U3Ed__10_System_IDisposable_Dispose_m0A0FE7DCABFC6D7BF2BE21405C43D4C3AFE38536 (void);
// 0x00000178 System.Boolean MoverPlayer_<Esperar2>d__10::MoveNext()
extern void U3CEsperar2U3Ed__10_MoveNext_mEE9E3C1E5CBC066709A2EE71313C67309C33A70F (void);
// 0x00000179 System.Object MoverPlayer_<Esperar2>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperar2U3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A0461B56DD631CA49C9C37825E9AE9221803D0F (void);
// 0x0000017A System.Void MoverPlayer_<Esperar2>d__10::System.Collections.IEnumerator.Reset()
extern void U3CEsperar2U3Ed__10_System_Collections_IEnumerator_Reset_m8BE91E883B66BDE039193A68571A342F6909CBA4 (void);
// 0x0000017B System.Object MoverPlayer_<Esperar2>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CEsperar2U3Ed__10_System_Collections_IEnumerator_get_Current_m030BE28626C30B2EE2EFED7668B47AB883A2B195 (void);
// 0x0000017C System.Void PlayerController_<Esperar>d__33::.ctor(System.Int32)
extern void U3CEsperarU3Ed__33__ctor_m8ECF8A8461A0F5A9A3B575D34E35445E039E5326 (void);
// 0x0000017D System.Void PlayerController_<Esperar>d__33::System.IDisposable.Dispose()
extern void U3CEsperarU3Ed__33_System_IDisposable_Dispose_m440AE8058280B535E1A6CC34B6015F1AEF2F6D08 (void);
// 0x0000017E System.Boolean PlayerController_<Esperar>d__33::MoveNext()
extern void U3CEsperarU3Ed__33_MoveNext_mA7E6D939465C64EA1579F9FD2219C9325FC673A3 (void);
// 0x0000017F System.Object PlayerController_<Esperar>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperarU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A0B5ED1ED86E0A5DC88D1C88D1B954A8CCDF80D (void);
// 0x00000180 System.Void PlayerController_<Esperar>d__33::System.Collections.IEnumerator.Reset()
extern void U3CEsperarU3Ed__33_System_Collections_IEnumerator_Reset_mDF4D945BDDE139D70C214CA536805E5F9BB7DC88 (void);
// 0x00000181 System.Object PlayerController_<Esperar>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CEsperarU3Ed__33_System_Collections_IEnumerator_get_Current_m50F97FB8F91ED8245C6E20F2BAAD7D9DADB548E4 (void);
// 0x00000182 System.Void PlayerController_<Esperar2>d__34::.ctor(System.Int32)
extern void U3CEsperar2U3Ed__34__ctor_m6C7D086F8D623D801383F74ACAF3722986047977 (void);
// 0x00000183 System.Void PlayerController_<Esperar2>d__34::System.IDisposable.Dispose()
extern void U3CEsperar2U3Ed__34_System_IDisposable_Dispose_mBA93BE59A4CE1DD6913E273D61623EABAA0C49FD (void);
// 0x00000184 System.Boolean PlayerController_<Esperar2>d__34::MoveNext()
extern void U3CEsperar2U3Ed__34_MoveNext_m394C75A6A32F8ABDBAD7F30F3DDB7A43ECE4C1D1 (void);
// 0x00000185 System.Object PlayerController_<Esperar2>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperar2U3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD361386F8EF3803943E936C464D0DC06FA4FA2F8 (void);
// 0x00000186 System.Void PlayerController_<Esperar2>d__34::System.Collections.IEnumerator.Reset()
extern void U3CEsperar2U3Ed__34_System_Collections_IEnumerator_Reset_m30E7B3079666B5535CB44E43D8D204CD276D2A5B (void);
// 0x00000187 System.Object PlayerController_<Esperar2>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CEsperar2U3Ed__34_System_Collections_IEnumerator_get_Current_m7CC662AD125622B21CA3F63BA1A4B08BDD87B23B (void);
// 0x00000188 System.Void Rotator_<Rotate>d__3::.ctor(System.Int32)
extern void U3CRotateU3Ed__3__ctor_mAFA1AEA6E3F7EACE3C599B421A87C4B6839D7D34 (void);
// 0x00000189 System.Void Rotator_<Rotate>d__3::System.IDisposable.Dispose()
extern void U3CRotateU3Ed__3_System_IDisposable_Dispose_m244C4F4AB2743977DB8C2815A6930D89C41D84BA (void);
// 0x0000018A System.Boolean Rotator_<Rotate>d__3::MoveNext()
extern void U3CRotateU3Ed__3_MoveNext_m63E8E6B5A6EF29630BBD30F252CCA37319E51A6D (void);
// 0x0000018B System.Object Rotator_<Rotate>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRotateU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1653C3A3E7DB369BBB6598854703607EA023D74 (void);
// 0x0000018C System.Void Rotator_<Rotate>d__3::System.Collections.IEnumerator.Reset()
extern void U3CRotateU3Ed__3_System_Collections_IEnumerator_Reset_m1106D07C76722C6FD541985BBFC6D5B96DE2D06C (void);
// 0x0000018D System.Object Rotator_<Rotate>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CRotateU3Ed__3_System_Collections_IEnumerator_get_Current_m71DEDCBD4F8978966FFB254FD5D512730D4736B2 (void);
// 0x0000018E System.Void SaltoSticky_<Esperar>d__4::.ctor(System.Int32)
extern void U3CEsperarU3Ed__4__ctor_mB084F0EA6347BB7C4A80B7BAD7B0DA822C1AC5D1 (void);
// 0x0000018F System.Void SaltoSticky_<Esperar>d__4::System.IDisposable.Dispose()
extern void U3CEsperarU3Ed__4_System_IDisposable_Dispose_mC72867539A2E4A4661C2458FB45FC4DE609F041B (void);
// 0x00000190 System.Boolean SaltoSticky_<Esperar>d__4::MoveNext()
extern void U3CEsperarU3Ed__4_MoveNext_mF574629DCFDDD2B0BA0F275E7A9E8659437DDE40 (void);
// 0x00000191 System.Object SaltoSticky_<Esperar>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEsperarU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB0747992314B2D66BBCAA6E9432A5771A34F640B (void);
// 0x00000192 System.Void SaltoSticky_<Esperar>d__4::System.Collections.IEnumerator.Reset()
extern void U3CEsperarU3Ed__4_System_Collections_IEnumerator_Reset_mB4E40D977B51661ADBE9D38E60E19AE8BAE8E549 (void);
// 0x00000193 System.Object SaltoSticky_<Esperar>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CEsperarU3Ed__4_System_Collections_IEnumerator_get_Current_mBEC9D2AC34F0598E71CC24A0CAFA58E646AE371F (void);
// 0x00000194 System.Void EnvMapAnimator_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m004DF17C34E6A9C76325BD4C65E0F328AD4B37E0 (void);
// 0x00000195 System.Void EnvMapAnimator_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mE7E20C5789828A8FDE263BD5ACC2D02982334B46 (void);
// 0x00000196 System.Boolean EnvMapAnimator_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mB3F55ABCABBA6717942BDC85935FDE439D09E226 (void);
// 0x00000197 System.Object EnvMapAnimator_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF976D094846BDC403335C76F41CAC26D53C8F1D2 (void);
// 0x00000198 System.Void EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9368B3951732B0D18993DA1B889346A775F252CE (void);
// 0x00000199 System.Object EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m495592283D84E6B446F7B9B7405F250D9687DEFB (void);
// 0x0000019A System.Void Readme_Section::.ctor()
extern void Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96 (void);
// 0x0000019B System.Void TMPro.TMP_TextEventHandler_CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m036DA7F340B0839696EB50045AB186BD1046BE85 (void);
// 0x0000019C System.Void TMPro.TMP_TextEventHandler_SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0BC042938C4EBBB77FFAD68C1ACD74FC1C3C1052 (void);
// 0x0000019D System.Void TMPro.TMP_TextEventHandler_WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m1C01733FD9860337084DFE63607ECE0EF8A450EA (void);
// 0x0000019E System.Void TMPro.TMP_TextEventHandler_LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m1C3A0C84C5C0FEA6C33FA9ED99756A85363C9EF2 (void);
// 0x0000019F System.Void TMPro.TMP_TextEventHandler_LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_mC7034F51586C51D1DE381F6222816DC1542AFF3A (void);
// 0x000001A0 System.Void TMPro.Examples.Benchmark01_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m139DF863E59AD287A6C14228BB59D56E7FD2E578 (void);
// 0x000001A1 System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mBBFAE2F68813477259A0B665B4E81833C03C746B (void);
// 0x000001A2 System.Boolean TMPro.Examples.Benchmark01_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m8D27A150B4BC045DD5A1ACB2FABBB7F7F318A015 (void);
// 0x000001A3 System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C55687F407BA372889D6A533FB817E1EA81C165 (void);
// 0x000001A4 System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mE725DFFE744FAEDABF70579D04BBEB17A6CFF692 (void);
// 0x000001A5 System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD8EEDE3D2E1E9838472D20AE93DD750D1EE39AF8 (void);
// 0x000001A6 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mECD4DB9695B4D04CEF08DF193DAFA21412DA40EF (void);
// 0x000001A7 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m6773C67C5C6E7E52F8C8545181173A58A6B7D939 (void);
// 0x000001A8 System.Boolean TMPro.Examples.Benchmark01_UGUI_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mB823F98B1B0907B6959EAEE4D1EBE0AA35795EA3 (void);
// 0x000001A9 System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF100CFC13DED969B3BBE2007B3F863DCE919D978 (void);
// 0x000001AA System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mDE1D3A30ECA00E3CA2218A582F2C87EEE082E2DB (void);
// 0x000001AB System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m13710692FCBB7BB736B1F6446778124064802C83 (void);
// 0x000001AC System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mB4DA3EEEFC5ECB8376EF29EAC034162B575961B8 (void);
// 0x000001AD System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mC6A68A27A902E234429E3706D6DB432BEA04A384 (void);
// 0x000001AE System.Boolean TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_mDE142F6B3AAB5916834A6820A5E7B13A7B71E822 (void);
// 0x000001AF System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9F4D59DDC372B977CD7297AA5F0C83D4FFBBA830 (void);
// 0x000001B0 System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mAC1E960F7FFCF032EE668635835954377D1469F6 (void);
// 0x000001B1 System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m96B6B8A0D715268DB789F3D4C60FC6DEC9E1F6E6 (void);
// 0x000001B2 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_m07871FDF578BC130082658B43FB4322C15F0909E (void);
// 0x000001B3 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m3C8BC1501397E256464ADD27A32486CDE63C2BE2 (void);
// 0x000001B4 System.Boolean TMPro.Examples.SkewTextExample_<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m11D4701B069FCFC106319CBDCA56244EFA4C795F (void);
// 0x000001B5 System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m995FCE4A3B9B270605168D01EF7439A437864C06 (void);
// 0x000001B6 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m9970CA113E1A293472987C004B42771993CAA05C (void);
// 0x000001B7 System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m398E9D0D09F26D5C3268EB41936ED92240474910 (void);
// 0x000001B8 System.Void TMPro.Examples.TeleType_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mFB2D6F55665AAA83D38C58F58FA9DD0F5CE51351 (void);
// 0x000001B9 System.Void TMPro.Examples.TeleType_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m72D91E3700B8E1F2053A7620793F97E01C1A2E3A (void);
// 0x000001BA System.Boolean TMPro.Examples.TeleType_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2839A135AFB4518430283897981AF4C91ABDBC6A (void);
// 0x000001BB System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E80ED07333F946FC221C16A77997DF51A80F87 (void);
// 0x000001BC System.Void TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mAC8E620F4FCE24A409840017FB003AA1048497EF (void);
// 0x000001BD System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mD6DFD0886CCA250CB95B2828DEEEEE5EA6DC303A (void);
// 0x000001BE System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mAF579198F26F3FD002CB7F4919CCB513E2B770E1 (void);
// 0x000001BF System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m2B3AB12689498DE28F90CF5DA3D40AA6C31B928E (void);
// 0x000001C0 System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_m16C2594A51B9D102B30646C47111F3476FFBF311 (void);
// 0x000001C1 System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B5325041D4E4A3F3D5575373F25A93752D9E514 (void);
// 0x000001C2 System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m321E1A1FD7DE7840F8433BF739D3E889FB3E9C7C (void);
// 0x000001C3 System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9CF6271A74900FFA5BD4027E84D63C164C1650BC (void);
// 0x000001C4 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m5D2D48675C51D6CBD649C1AAD44A80CCA291F310 (void);
// 0x000001C5 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m9F97B9D63E30AF41B31E44C66F31FE6B22DDFD4C (void);
// 0x000001C6 System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m19EB0E15617A1893EEF1916629334CED1D8E9EA1 (void);
// 0x000001C7 System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m98E45A0AFB76FA46A5DA4E92E3FE114E310D8643 (void);
// 0x000001C8 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m09CF6739322B1DB072CB8CFE591DBBC046008E63 (void);
// 0x000001C9 System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m2083C1001867012CC47AFA8158F00ED15527C603 (void);
// 0x000001CA System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m7E5E121E510EFDCCF0D565EBBF607F80836EBB66 (void);
// 0x000001CB System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_mD0C99DF97552E843D1E86CF689372B3759134BB7 (void);
// 0x000001CC System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_mA0FD89A2C5D0867977DAB0896DAB041EDFA64200 (void);
// 0x000001CD System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m735F43BD7983BCC8E417573886ADC91CDED8F29B (void);
// 0x000001CE System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mBE24EA2EDAFC5FD22D633595174B122D5F84BB02 (void);
// 0x000001CF System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m4199EEF69BC21DC6DDC2A0D0323AB70508CB1194 (void);
// 0x000001D0 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m664F9327A457FB4D53F20172479BB74A4B50675A (void);
// 0x000001D1 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_mA301D3B10770F41C50E25B8785CE78B373BBCE7C (void);
// 0x000001D2 System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m96E4CEDBAD5AA7AC7C96A481502B002BC711725F (void);
// 0x000001D3 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7609D75AFDAE758DAAE60202465AB3733475352B (void);
// 0x000001D4 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m6F0C933685FE89DF2DECC34EDD2FE5AD10542579 (void);
// 0x000001D5 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m29F3BB8ADAADB8FD750636C535D2B50292E2DE3B (void);
// 0x000001D6 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_mD05EA47C6D3F9DC7BE5B4F687A62244E48BC3808 (void);
// 0x000001D7 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5E0C78AE94BC2C9BD7818EF0DD43D46ABF8C0172 (void);
// 0x000001D8 System.Boolean TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m8ABBF64D39EB5C5AC87AE0D833B6CBE570444347 (void);
// 0x000001D9 System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m328E88E5192D85CDD2157E26CD15CC0B21149AB6 (void);
// 0x000001DA System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_mB49B7C6E42E5B1F488ECA85B1B79AC1D6BBC3022 (void);
// 0x000001DB System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m6966F60B8B8F4540B175D0C145D8A73C89CE5429 (void);
// 0x000001DC System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m14F85BFAE5EFFAF0BBD6519F6A65B1EE36FC5F0F (void);
// 0x000001DD System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m36A05E300F1E06AE13E2BFF19C18A59F3E73424A (void);
// 0x000001DE System.Boolean TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m95629D41B32EBFFE935AA88081C00EB958D17F53 (void);
// 0x000001DF System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m994AE653E10219CEB601B88CC22E332FF8AD1F36 (void);
// 0x000001E0 System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mE8D9B5795F095798688B43CF40C04C3FE3F01841 (void);
// 0x000001E1 System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m12F2FCDBABCD9F41374064CAD53515D2D039EFD0 (void);
// 0x000001E2 System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m93C153A3293F3E7F9AB47C712F5D8BC9CB0B179D (void);
// 0x000001E3 System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m6E2E964131F208551826E353B282FBB9477CB002 (void);
// 0x000001E4 System.Boolean TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mD5F89478A4FDEA52E1261CCCF30CC88B0D693407 (void);
// 0x000001E5 System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6A7BC75B026CFA96FA1C123E6F7E5A5AAFC46E9 (void);
// 0x000001E6 System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m28679C8DDFAAD809DD115DB68829543159F47FBD (void);
// 0x000001E7 System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m85E44906BDBB53959D8A47AE74B74420179AD3EC (void);
// 0x000001E8 System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m9B592C7897687114428DF0983D5CE52A21051818 (void);
// 0x000001E9 System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m24A98FB24DAE578F96B90B3A6D61D18400A731B1 (void);
// 0x000001EA System.Boolean TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m52F01BB7305705B9DE4309E1A05E2725BA06672E (void);
// 0x000001EB System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB05E3BDEFD461F6516F45D404DBDEC452C646D37 (void);
// 0x000001EC System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m72086FED04B8AC40B92836B894D6807AFB51BB38 (void);
// 0x000001ED System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mA1E44018FA1464D953241F5FA414715C9ADE98CF (void);
// 0x000001EE System.Void TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mF154C9990B4AF8DA353F6A8C115C96FB7CB76410 (void);
// 0x000001EF System.Int32 TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_mDEE1E28BD53D02CB2E40D0C263BBA65C0B5AC66C (void);
// 0x000001F0 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m92C755B17AC00199A759541B62E354765068E7F1 (void);
// 0x000001F1 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mBA95580B1808E4EB047CD7F082595E1C5EE930C2 (void);
// 0x000001F2 System.Boolean TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mA307F1943028D7555032189B48F6289E09D2E220 (void);
// 0x000001F3 System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288CCD0BF91027A0CE3AE12D301F3E5189F0DCE (void);
// 0x000001F4 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m7C214643A52D954B2755D3477C3D0BFC85DAFF8E (void);
// 0x000001F5 System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m666CA82A5C974B5AC09F277AFC97A82D1D1C365E (void);
// 0x000001F6 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m63F411BEA2E513D84AAA701A1EDF0D0322FDE9C4 (void);
// 0x000001F7 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m3D9631AF92186316351AFF095E5176423B84E25F (void);
// 0x000001F8 System.Boolean TMPro.Examples.WarpTextExample_<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m24ED53830B643858F6068C6908E7C0E044C671A4 (void);
// 0x000001F9 System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07B1ABC445F7EADD1DB5F513857F1607C97AAC12 (void);
// 0x000001FA System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4C7C46CC80C502CC1AD61D2F9DAF34F09226AF06 (void);
// 0x000001FB System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m86693688EEF5E754D44B448E82D87FAB40D6C8B8 (void);
// 0x000001FC System.Void UnityTemplateProjects.SimpleCameraController_CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_m6467352ED87301E5F4A76456060A765CAB96AF3E (void);
// 0x000001FD System.Void UnityTemplateProjects.SimpleCameraController_CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_m76BCC104A48EA7F125D5A50D874A2DEEA7967247 (void);
// 0x000001FE System.Void UnityTemplateProjects.SimpleCameraController_CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController_CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_m883AAF2D3C7F5045B64CAF655FB84EF0FC98F282 (void);
// 0x000001FF System.Void UnityTemplateProjects.SimpleCameraController_CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE3349362276789C1617C01276F7DE533BBA22623 (void);
// 0x00000200 System.Void UnityTemplateProjects.SimpleCameraController_CameraState::.ctor()
extern void CameraState__ctor_m4A83DF36C7D280050EA1B101E61B7E345C31A322 (void);
static Il2CppMethodPointer s_methodPointers[512] = 
{
	Ahogados_OnTriggerEnter_m92E1A0441373E7139D78BFF21DE40ED148D6D23F,
	Ahogados__ctor_mD3FE7716DEE41C0487BB03297BEE04B3677494B7,
	BotonAzul_Start_m2D4D7029A019DA358D7BB53A7841D88DC7F767F7,
	BotonAzul_OnCollisionEnter_m7DC65CE655E15C9F6438B58489B107BFEA4195BB,
	BotonAzul__ctor_m572B1C62B1097E4D72F69E2C93E5BB5712949017,
	ActivarPor_Start_m5C22223A45AF1728A24BB54A7C4676F76FEB8D19,
	ActivarPor_OnCollisionEnter_m0C754886B8198350455EFB293140CF4662759D5F,
	ActivarPor__ctor_m270F9A44576DF0C592AB81E5F5A35A1E93964031,
	Green_Start_mF98228230F906C86436A24EB60AF10C48B55BA37,
	Green_OnCollisionEnter_m3A8C494BCA75938B3A0198BEDDA3C36A954C46E5,
	Green__ctor_m00835381DE782F47DE5DBAEA4F80CFD9F78291FD,
	Yellow_Start_m198862B08031FBA2FA63705DB34497B8B02F0A5B,
	Yellow_OnCollisionEnter_mA0033E306B64DCA642028DF34DB8CE856B485EAB,
	Yellow__ctor_m0FE3C71BC14263EDEBDD94305A33F7D34D475542,
	bluee_Start_m9114C7CF85F0BF44D7ACF035EEBFCF7B1C87A5D1,
	bluee_OnCollisionEnter_mD1997AAB0D5597F9E8B7AF8978530CD0B700327C,
	bluee__ctor_m384C1A168A56BA3E56FF2AEBDE1F1ADF6CAEB65E,
	Pink_Start_m44C106B354A14C2447B8072FF6B66C90536C67BD,
	Pink_OnCollisionEnter_mB6ED49ED1716655D67B78FE1DC39F7D5F41050A4,
	Pink__ctor_m3CD82F261C898520E38C105C22F807CE6A8202A7,
	Blue_Start_mFF5AE3694F3A3A3E86491B1FB6395838EFB73802,
	Blue_OnCollisionEnter_m0C2F4B8A3971B0EBD352BDCF8FBD5C9393D3A8C4,
	Blue__ctor_mDFBC60FA30AAE0BD527942FE808EF332F68E3352,
	boton_Start_m4FD740A5C49E167D609A5D2B2976DBAB4CE1DE12,
	boton_OnCollisionEnter_m93CA49978E15DE841A423E1F9576614C77F6F8CC,
	boton__ctor_m5B5518026F57C5BB5123ADB104189C49C7501B31,
	botonn_Start_m43EBDF7598048D33A2C39A6BF98E995C6523C4B6,
	botonn_OnCollisionEnter_m08DCA7099BB99EB940C90BA032E66D5B67CE0173,
	botonn__ctor_mAE820DD557A405F487C109BCC562D54D58BE2950,
	botonbrown_Start_m894EA319FBD22D6E7CBF38C430CF3767547E4FA0,
	botonbrown_OnCollisionEnter_m1FBCA1E0ABB56749CEC6BFBEB7253496BBEE9361,
	botonbrown__ctor_mA57A0ECA303F5839869CE70D1D20C90892DC88FD,
	ChangeMaterialColor_OnTriggerEnter_m72BE367AA34955A64300A3731126416074F26050,
	ChangeMaterialColor__ctor_mA7C41E8B9D59373CD22D20B5B166CF3005B02BEC,
	ControlCamara__ctor_m954699EF3390352A462FE175B54EEB6A85882E34,
	DesactivateWater_OnTriggerEnter_mEB78F004B8348CB13AAEBF0E702B53F5264931A0,
	DesactivateWater__ctor_m4E36BED2656EAF4D31850E5F4349638AB20E8F71,
	DisparoVioleta_Start_mC59801011862F215FC5ABC26A738B102D0600DED,
	DisparoVioleta_Update_m42A76DA63F6872D2A1BFE8397E3CFFCC404AF212,
	DisparoVioleta__ctor_mFC08BB359B83DB0F95A08FD01FD0421EA4B141C5,
	Explocion1_Start_mDFCC3B7578CEFC3895ECA8D13FC8A84BD5152EC5,
	Explocion1_Main_m71D3468C97B21DFD48540F165D9A4649DF019686,
	Explocion1_crearcubo_m3D9D04598A03AE173F0737C86E80282740BB651B,
	Explocion1__ctor_mA6B2EB72BBB5B7342EE1FCB7E44772DBB83D6323,
	Plataforma_Start_m5C2F13758A64BD7C311D4FB97F05F2018A0D21BD,
	Plataforma_OnCollisionEnter_m0CEFDF54C468EBE694B6E8F6EE952AEA782E3530,
	Plataforma__ctor_m0731064900D4E868852A3238F25E9391B71D9DAB,
	LASER_Start_mF867932299EC97FC9BBFB21AE90B44A246A1E37E,
	LASER_Update_m22C0ACEEC6E7236105D0FEA7BC761F7E4E189BF5,
	LASER_Esperar2_m848091CE519478B6FA9637CFD429D60C412AC788,
	LASER__ctor_m7A535AE2240091690D78B0EAD366915082FF0F0F,
	Crecimiento_Start_m26E908274442CE06AA3171F41897E87D227BFD57,
	Crecimiento_Esperar_m4FDC9F89F56BD8F06FC1744225659F700B1CA32E,
	Crecimiento__ctor_mC85A12B6EC73AB27F3E72D2C9E3F7856253525B9,
	Detecta_Start_m9D0CBC13F5F8C37CF26A6BA10AAF2CD4CA8C6672,
	Detecta_Update_mE03AA3ED436D0B883DB629970EAD52E5C2A8BC6E,
	Detecta_Esperar_m60EB3101E815859B720ED2EF59076A9B5650B3CD,
	Detecta__ctor_mA155C97B7A04E810C4426A360393DB1C081FC2AA,
	Control_Start_m471D7E4645930F8FBA139A65636AD195FFA537C0,
	Control_Update_mC0043627EBF0478227B4FA58974F4A1308F26B7E,
	Control_OnCollisionEnter_m2EB88A721B7D9ECA37F04E372F8C3E5C532B1D2E,
	Control__ctor_mD9E42706311F50254A186C325E48A355ADE1FFEB,
	Explocion2_Start_mDA5052067A9A668974248262F3C33447AE5EEF9A,
	Explocion2_Main_m5359A8A6D940D21BBB652759ED060F5268AAD199,
	Explocion2_crearcubo_m48BD09B4009A560C81A50FFA737313875FE6EE02,
	Explocion2__ctor_mBCBD67791CA8C8BDB8C2182DA5B78F866F89AE32,
	Disparar_Start_m727BA1F0BEED0F432A750F29E06C9718FBC1EE1B,
	Disparar_Update_m4B12877AE93E05207B9664473DA29C6C24992E3E,
	Disparar_Esperar2_mDC9FC16456811777C5D645B9DCC6CE3814A12794,
	Disparar_Esperar_m3375FA8E2B9457F2DDA2E7B2BB51EA5A3B29B261,
	Disparar__ctor_mD25E1D3ABEFCDCAF4CED7C35156666CD7B2B64EF,
	seguirjugador_Start_m81766263DA3ED4933AB1140FA50057C1587355E2,
	seguirjugador_Update_m452BEEB3D32D3D9142DA7D17F196910E55EF9BE9,
	seguirjugador__ctor_mFB9C0085235BC22AF30AC10CCF53FC9EB9B4473E,
	Tp_Start_mA1B091B30AD514AF13CF22B23CD8145999594751,
	Tp_Update_m29441DD821897E7DF8E9C9A79F05398CF31D9417,
	Tp_logica_m0457CDE3CDAAC70EA08DA71E328CD79E581D059B,
	Tp_CTP_m0C60E622B9087CB375BDB17E3424BBA009B737D1,
	Tp_Esperar_m3E62D720E0A27EE67E2305EE2A55288BD3F68FBF,
	Tp__ctor_mA5872DFE2DD46B7BEE3E4F0EBEA6B8CB528770BA,
	SeguirPJ_Start_mA115A41773012E74514757EDB4A0F7FD679674C7,
	SeguirPJ_Update_m6F489BB363687DA0E8F9B4BDB7FA111C6B415529,
	SeguirPJ_OnTriggerEnter_m4063E3877DC8266A941ED5973B87D9E9E6A02F70,
	SeguirPJ_esperar_m4C44297E9A9A5B7AB9C3927153D587A8DE69BAB9,
	SeguirPJ__ctor_m9D7D2B70670B5214395C765429303EE95933224D,
	Explocion_Start_m1408D753813A0AA0715BD911BB184D4C86E181F7,
	Explocion_Main_m7CEE1A7AC344B65E6A5B4DD240E19F14EDA6282D,
	Explocion_crearcubo_m6869D2FE6115BF2E420765F62A485EC1BC208655,
	Explocion__ctor_mD0F385BDB7C39E25FD4010028152F3ECEB641F7A,
	GraplingGun_Awake_m2A1B402D8783947AD7CC81AB631A60BD54BF4D54,
	GraplingGun_Update_m5A65E3F67F696D16B8E6AA301ADDDAFCE2FB23A6,
	GraplingGun_LateUpdate_m0D71BB9C5885827C4F50826E7A61B17DFD1B3A94,
	GraplingGun_StartGrapple_m4804421378CA74FDED27B71EEDFF8177BA948540,
	GraplingGun_DrawRope_mFF37EA0CA9159763A04F0515F56665D6B11D1CB1,
	GraplingGun_StopGrapple_mAEF998B55B590678492C22910D2DCD95B8AB49EA,
	GraplingGun__ctor_m2589EA13EF8C258FF935CBE9F19BC5357197F5C0,
	MoverPlayer_Start_m8D3684BA62CE1B8A2986E314FEE55A805E2FEC08,
	MoverPlayer_Update_m06DE8FC334EFBEA2F0DA3893664D1DABC73F2E71,
	MoverPlayer_OnCollisionEnter_m4BCCB3348FA2F190D289F2C6C927848FF7A1507E,
	MoverPlayer_Esperar_m79A587D4FF47E1BC9B9954FCFE0A2121160DAD87,
	MoverPlayer_Esperar2_mBA0F26B2A1CB5B1C0F3D5ABB5D455D1FC7AD413C,
	MoverPlayer__ctor_mA450D7E979FB85DAEECFE3D675596EFD97E07A0E,
	ParedRompe_OnCollisionEnter_m88CC385C99AFA3D653EED69A58D05FA7D3FE070F,
	ParedRompe__ctor_m48AC4E1CBE378DD7453D29CFC4253E0054250758,
	PlatformUpDown_Awake_m646621BD90F4BA843E77C0F27DF7EA0747C35418,
	PlatformUpDown_Start_m838577C75B95297CBC35B21EA3A1D84AA1CA8896,
	PlatformUpDown_Update_mFF687F764172AA6321F98F3BDFAA9BBD65705F48,
	PlatformUpDown_OnDrawGizmos_m4EFE80E284A111C9F48E5F9B69B6179211D0E3C6,
	PlatformUpDown__ctor_m513798FEC40D8E965179251A721C30E75619902C,
	PlayerController_Start_m37E4A9612CC78B422FBFC15D257767AE6F969D78,
	PlayerController_Update_mE64911CD78BC318BCCB0EDB3D9DF3BC1559C70FA,
	PlayerController_EstaEnPiso_m7AF1F86892D2A4928A322E029F2B254D998E0386,
	PlayerController_OnCollisionEnter_mC8B062742ECE79680260B372811F8997C4502945,
	PlayerController_Esperar_mB5ED640C7329FED5D4A3C07E9669EB8317447068,
	PlayerController_Esperar2_m99FD955D4865C6D0ED2BEBFDB5ADF40F3BAE64CB,
	PlayerController_OnTriggerEnter_m1310BAC5FA7FAE003E04BFDB6173885B41D826E7,
	PlayerController__ctor_mF66473FEFF2DFEB0325A4D9C917C27C6CE4B22F7,
	Reinicio_Start_m117003C8C17E0C7DCEA1A8B7013FAA76887B5103,
	Reinicio_Update_m5904E60C7BD1E64EBA58E66B2650D0CD4346A6FF,
	Reinicio_OnCollisionEnter_m74F6394ACF451651F577BF3958E9588454A8106E,
	Reinicio__ctor_m58C0897293235E3D1EBBC98729F4E3C64204E2EE,
	RotadorVert_Update_m9B9D6D1E0C7B30C7C4C70DFC2AD9188030D8C79A,
	RotadorVert__ctor_m87D89EFA72E5B78CB0DD221903719ED30517C33C,
	Rotator_Update_m5EEA78DA43EE3059DD3A2B5E0A378709E0DF21FA,
	Rotator_Rotate_m005E1B20EB4BFD7E784152E464D0AF3C6E93E526,
	Rotator__ctor_m6FA12381051396D6275FE1B56FBD254201D5B127,
	SaltoSticky_Start_mD6E26E22872CE0CCB36E46CA0ED3899269E5A516,
	SaltoSticky_Update_mF74AF0F24816874518F7D822D3F7911FE443D4BC,
	SaltoSticky_Esperar_mD6D079D9EA09532EC93C96B5764148E669027A31,
	SaltoSticky_OnCollisionEnter_m6CAA642B0BF11AE72C60FA0F5690F33999715FC6,
	SaltoSticky__ctor_m722CAF08619C4A7C903967D25C2BDAFE735C1984,
	Sticky_Start_m456A9F1487622BFC2DE779A6A0B6CA7FE862FB96,
	Sticky_OnCollisionEnter_m9284543F1145ECF54E9A88BB4984B6B7541CD94A,
	Sticky__ctor_m42CB9AC8C971ADFB195FA1E7CFD1AF96A262F86F,
	Teleporter_OnTriggerEnter_m5C18DC16B0D477649D35CB1D121CAA11BD842B52,
	Teleporter__ctor_m5AC57ECFE09407D61BD1454D78EA0861002D1B86,
	pjgrav_Start_m0B1A369C745645EFA180E19BDE2EA696837399B7,
	pjgrav_Update_m9A16C9D3BC4B84452EA6661798F86DF8D503DB55,
	pjgrav_OnTriggerEnter_mD31E63D2100AC96F0228810518BB386455BA0335,
	pjgrav__ctor_mEC3D411B7FD06A9A446E10C401962E705DA376A1,
	pjpare_Start_mF1DDD04C107F9EACEF2F922F50C6D7069AB7CB2C,
	pjpare_Update_mD7F21C2BF6C114C2CC1AFE5BAB899C668EF6C90F,
	pjpare_ponerpare_m9A6DABBB0AF89FB9387FC430262A4590A6409A6B,
	pjpare_OnTriggerEnter_m1769393C3448816C7E70DCBD1371FB4CBDF5DA7B,
	pjpare__ctor_mCF0461F6FB0158F03E8B5C4B7A1C3CCB40C05AC9,
	ChatController_OnEnable_mD13A02B63932BDA275E1A788FD72D86D69B9E440,
	ChatController_OnDisable_mD31D1ED1B2C3C82986BFBD18FA584D45167431F3,
	ChatController_AddToChatOutput_m01CC3E959ACECC222DFD8541231EC1E00C024194,
	ChatController__ctor_mF4343BA56301C6825EB0A71EBF9600525B437BCD,
	DropdownSample_OnButtonClick_m31A59FA78B78A6457BB20EC10C08C82E4C669140,
	DropdownSample__ctor_mA06B4508E4574A97B9B0A20B0273557F5AD068D7,
	EnvMapAnimator_Awake_mAB3C67FA11192EFB31545F42D3D8AAB2A662FEB2,
	EnvMapAnimator_Start_m21098EFD0904DFD43A3CB2FF536E83F1593C2412,
	EnvMapAnimator__ctor_mBA9215F94AB29FBA4D3335AEA6FAB50567509E74,
	Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C,
	laser_OnTriggerEnter_m40C7D3A94A0C6E0797A5FC87DB26928C0AD0A1FC,
	laser__ctor_m56644DC99EE860E092DCF6CF0A0A06E728F384F9,
	TMP_DigitValidator_Validate_mD139A23C440A026E47FA8E3AD01AD6FEF7713C3D,
	TMP_DigitValidator__ctor_mF6477F5EB75EC15CD6B81ACD85271F854BABC5D6,
	TMP_PhoneNumberValidator_Validate_mCA5EA200223A9F224F2F4DBD306DAE038C71A35F,
	TMP_PhoneNumberValidator__ctor_mBB38130850945A40631821275F07C19720E0C55E,
	TMP_TextEventHandler_get_onCharacterSelection_mF70DBE3FF43B3D6E64053D37A2FADF802533E1FF,
	TMP_TextEventHandler_set_onCharacterSelection_m237C99FE66E4E16518DAE68FF9CBF1A52E816AD2,
	TMP_TextEventHandler_get_onSpriteSelection_m395603314F8CD073897DCAB5513270C6ADD94BF4,
	TMP_TextEventHandler_set_onSpriteSelection_mAAE4B440E34EE5736D43D6A8A7D3A7CEE0503D69,
	TMP_TextEventHandler_get_onWordSelection_m415F4479934B1739658356B47DF4C2E90496AE2E,
	TMP_TextEventHandler_set_onWordSelection_m6062C0AF2FDD8752DC4A75663EE8E5C128504698,
	TMP_TextEventHandler_get_onLineSelection_m8E724700CC5DF1197B103F87156576A52F62AB2B,
	TMP_TextEventHandler_set_onLineSelection_m1A8E37D2069EF684EF930D4F1ABE764AE17D9A62,
	TMP_TextEventHandler_get_onLinkSelection_m221527467F0606DD3561E0FB0D7678AA8329AD5D,
	TMP_TextEventHandler_set_onLinkSelection_m1376CC9B70177B0C25ACEDF91D5B94BC4B8CF71D,
	TMP_TextEventHandler_Awake_m9A353CC9705A9E824A60C3D2D026A7FD96B41D74,
	TMP_TextEventHandler_LateUpdate_m2F3241223A91F9C50E11B27F67BA2B6D19328B72,
	TMP_TextEventHandler_OnPointerEnter_m1827A9D3F08839023DE71352202FE5F744E150EF,
	TMP_TextEventHandler_OnPointerExit_m788B93D2C3B54BCF09475675B274BCB047D449FB,
	TMP_TextEventHandler_SendOnCharacterSelection_mBC44C107A6FB8C43F7C6629D4A15CA85471A28B2,
	TMP_TextEventHandler_SendOnSpriteSelection_mEF24BCE06B0CE4450B6AE9561EC4B5052DAF00F6,
	TMP_TextEventHandler_SendOnWordSelection_m7C4D266070EE2ADC66BCCFD50EB74FEB4923B77E,
	TMP_TextEventHandler_SendOnLineSelection_mAAF4AF44929D0C9FD73C89E5266028908074AEB1,
	TMP_TextEventHandler_SendOnLinkSelection_m082D12F7D044456D8514E4D31944C6900F8262C0,
	TMP_TextEventHandler__ctor_mEA56AE9489B50CF5E5FC682AA18D1CE9AF8E1F8B,
	Benchmark01_Start_mC0055F208B85783F0B3DB942137439C897552571,
	Benchmark01__ctor_m2FA501D2C572C46A61635E0A3E2FF45EC3A3749C,
	Benchmark01_UGUI_Start_m976ED5172DEAFC628DE7C4C51DF25B1373C7846A,
	Benchmark01_UGUI__ctor_mD92CA5A254960EB149966C2A3243514596C96EAD,
	Benchmark02_Start_m2D028BFC6EFB4C84C1A7A98B87A509B27E75BA06,
	Benchmark02__ctor_m7CA524C53D9E88510EE7987E680F49E8353E4B64,
	Benchmark03_Awake_m8D1A987C39FD4756642011D01F35BDC3B1F99403,
	Benchmark03_Start_m73F65BA012D86A6BE17E82012AE8E2339CA5D550,
	Benchmark03__ctor_m9A5E67EA64AAC56D56C7D269CC9685E78276360A,
	Benchmark04_Start_m22D98FCFC356D5CD7F401DE7EDCDAF7AE0219402,
	Benchmark04__ctor_mDAC3E3BE80C9236562EFB6E74DEBE67D8713101D,
	CameraController_Awake_m581B79998DB6946746CBF7380AFCC7F2B75D99F7,
	CameraController_Start_mA9D72DB0BB6E4F72192DA91BC9F8918A9C61B676,
	CameraController_LateUpdate_mDC862C8119AB0B4807245CC3482F027842EAB425,
	CameraController_GetPlayerInput_m198C209AC84EF7A2437ADB2B67F6B78D12AB9216,
	CameraController__ctor_m2108A608ABD8EA7FD2B47EE40C07F4117BB7607E,
	ObjectSpin_Awake_mDA26D26457D277CC2D9042F3BD623D48849440C4,
	ObjectSpin_Update_mC50AAC1AF75B07CD6753EA3224C369E43001791B,
	ObjectSpin__ctor_m2832B9D713355ECF861642D115F86AA64A6F119E,
	ShaderPropAnimator_Awake_m8E01638EBFE80CC0B9E4A97AB809B91E3C6956BE,
	ShaderPropAnimator_Start_m24F4FADC328B0C76264DE24663CFA914EA94D1FD,
	ShaderPropAnimator_AnimateProperties_mC45F318132D23804CBF73EA2445EF7589C2333E9,
	ShaderPropAnimator__ctor_mC3894CE97A12F50FB225CA8F6F05A7B3CA4B0623,
	SimpleScript_Start_m22A3AE8E48128DF849EE2957F4EF881A433CA8CB,
	SimpleScript_Update_m742F828A2245E8CC29BC045A999C5E527931DFF1,
	SimpleScript__ctor_mA2284F621031B4D494AC06B687AF43D2D1D89BD7,
	SkewTextExample_Awake_m51C217E0CB26C2E627BA01599147F69B893EF189,
	SkewTextExample_Start_m6A9CEFA12DB252E297E41E256698DD4E90809F6A,
	SkewTextExample_CopyAnimationCurve_m3CE7B666BEF4CFFE9EB110C8D57D9A5F6385720B,
	SkewTextExample_WarpText_m4A69C47EA665D49482B930F924E49C8E70FAC225,
	SkewTextExample__ctor_m11DC90EB1A059F4201457E33C4422A7BDA90F099,
	TMP_ExampleScript_01_Awake_m9CE8A9F929B99B2318A6F8598EE20E1D4E842ECD,
	TMP_ExampleScript_01_Update_m8F48CBCC48D4CD26F731BA82ECBAC9DC0392AE0D,
	TMP_ExampleScript_01__ctor_m9F5CE74EDA110F7539B4081CF3EE6B9FCF40D4A7,
	TMP_FrameRateCounter_Awake_m906CC32CE5FE551DF29928581FFF7DE589C501F2,
	TMP_FrameRateCounter_Start_m614FA9DE53ECB1CF4C6AF6BBC58CE35CA904EB32,
	TMP_FrameRateCounter_Update_m16AB65EF6AB38F237F5A6D2D412AB7E5BF7B1349,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m537A709F25C3AA752437A025BEE741BD2F71320E,
	TMP_FrameRateCounter__ctor_mD86AC3A8D918D14200BF80A354E0E43DC5A565A2,
	TMP_TextEventCheck_OnEnable_m22D9B03F3E1269B8B104E76DA083ED105029258A,
	TMP_TextEventCheck_OnDisable_m42813B343A1FDD155C6BFBFCB514E084FB528DA0,
	TMP_TextEventCheck_OnCharacterSelection_mC6992B7B1B6A441DEC5315185E3CE022BB567D61,
	TMP_TextEventCheck_OnSpriteSelection_mEC541297C2228C26AB54F825705F0476D45F877A,
	TMP_TextEventCheck_OnWordSelection_mA1170F805C77CC89B818D8FBEE533846AF66509C,
	TMP_TextEventCheck_OnLineSelection_mB871339347DCB016E019F509A00BDE9A58105822,
	TMP_TextEventCheck_OnLinkSelection_m44A79DDBDF03F254BAFB97BE3E42845B769136C5,
	TMP_TextEventCheck__ctor_mA67343988C9E9B71C981A9FFAD620C4A9A6AA267,
	TMP_TextInfoDebugTool__ctor_m1EA6A5E31F88A1C7E20167A3BCCE427E9E828116,
	TMP_TextSelector_A_Awake_m82972EF3AF67EAAFD94A5EE3EA852CE15BE37FC1,
	TMP_TextSelector_A_LateUpdate_m40594D716F53E6E5BC0ECD2FFE8ECA44FAA5C8E4,
	TMP_TextSelector_A_OnPointerEnter_m8462C2DC4F71BDE295BE446B213B73F78442E264,
	TMP_TextSelector_A_OnPointerExit_m3AC7467ECE689A58590DA325F8B300B08C1E1B5D,
	TMP_TextSelector_A__ctor_m081D44F31AA16E345F914869A07BD47D118707DF,
	TMP_TextSelector_B_Awake_m217B6E2FC4029A304908EE9DC1E4AA2885CBF8A3,
	TMP_TextSelector_B_OnEnable_m24A7CCA0D93F17AC1A12A340277C706B5C2F9BAB,
	TMP_TextSelector_B_OnDisable_m6088452529A70A6684BD8936872B71451779A2F4,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m79EEE4DF7792F553F5DEDCF0094DAC6F2A58137A,
	TMP_TextSelector_B_LateUpdate_m745577078D86EF6C23B914BD03EA1A1D169B9B7B,
	TMP_TextSelector_B_OnPointerEnter_mF6C09A2C64F5D2619014ADD50039358FAD24DB3E,
	TMP_TextSelector_B_OnPointerExit_mB0AAA8D034FC575EB3BCF7B0D4514BD110178AD3,
	TMP_TextSelector_B_OnPointerClick_m13B20506F762769F099DE10B3CCA2DF194192B42,
	TMP_TextSelector_B_OnPointerUp_mD53FD60E0C5930231FB16BDEA37165FF46D85F6E,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m1D03D0E14D6054D292334C19030256B666ACDA0E,
	TMP_TextSelector_B__ctor_m494C501CF565B1ED7C8CB2951EB6FB4F8505637F,
	TMP_UiFrameRateCounter_Awake_m255A7821E5BA4A7A75B9276E07BC9EA7331B5AA6,
	TMP_UiFrameRateCounter_Start_mA4A02EB5C853A44F251F43F0AD5967AE914E2B0F,
	TMP_UiFrameRateCounter_Update_m04555F8DF147C553FA2D59E33E744901D811B615,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mDF0FDFBCD11955E0E1D1C9E961B6AD0690C669ED,
	TMP_UiFrameRateCounter__ctor_m99ACA1D2410917C5837321FC5AC84EAED676D4CC,
	TMPro_InstructionOverlay_Awake_m639E300B56757BDB94766447365E1C94B5B83ACD,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m74B8F0AE15DA6C9968C482981EBCF5CC9DB1F43D,
	TMPro_InstructionOverlay__ctor_m570C4B4CB3126622D6DFF71158336313C45C717A,
	TeleType_Awake_m5F758974DA88ED8187E71A5100D2D9E47985E359,
	TeleType_Start_mC32B726B6202883E12E1A62A52E50092E7E9D9F0,
	TeleType__ctor_m6CDFDC88D47FE66021C133974C8CB0E16B08A00E,
	TextConsoleSimulator_Awake_m4F61F06DFE11CFAF9B064CCA5B2D6423D5CFC302,
	TextConsoleSimulator_Start_m572903C9070A8AD276D2CB14DF7659AE551C75B3,
	TextConsoleSimulator_OnEnable_m1A36B043E4EDD945C93DFC49F7FAFB7034728593,
	TextConsoleSimulator_OnDisable_m6F9BE1975CB15EE559D3B617E8972C8812B41325,
	TextConsoleSimulator_ON_TEXT_CHANGED_m73C6B3DAA27778B666B9B3B75C9D4641FC1BEC8A,
	TextConsoleSimulator_RevealCharacters_mC24F1D67B99F0AFE7535143BB60C530C8E8735F0,
	TextConsoleSimulator_RevealWords_m68CAA9BDC1DF3454ECB7C5496A5A2020F84027B8,
	TextConsoleSimulator__ctor_m4DB9B9E3836D192AA7F42B7EBDC31883E39610E9,
	TextMeshProFloatingText_Awake_mD99CC6A70945373DA699327F5A0D0C4516A7D02A,
	TextMeshProFloatingText_Start_m3286BB7639F6042AD4437D41BF4633326C4290BE,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m61ABFBAA95ED83A248FBCC3F5823907824434D34,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m65A61DF0BA640F2787DD87E93B8FD9DEC14AACB4,
	TextMeshProFloatingText__ctor_m11DBC535BA8001B48A06F61515C0787C3A86611F,
	TextMeshSpawner_Awake_mB3C405B4856E9B437E13E4BD85DAE73FFF1F6561,
	TextMeshSpawner_Start_m436D4CD2A82C0F95B8D942DF4CCFCE09792EDF0F,
	TextMeshSpawner__ctor_mDB693DABF1C3BA92B7A3A4E1460F28E3FAFB444D,
	VertexColorCycler_Awake_mAEEAA831C084B447DFD0C91A5FA606CB26D3E22A,
	VertexColorCycler_Start_mF01B64B3E5FE5B648DE2EED031962D9183C2D238,
	VertexColorCycler_AnimateVertexColors_m9CDB87631C324FB89FA7D52F5BC910146F3DDEB7,
	VertexColorCycler__ctor_m9B68D69B87E07DC0154344E5276EFC5B11205718,
	VertexJitter_Awake_m9699A62E72D3A262EDFDF7AC63ABD33E53F179B6,
	VertexJitter_OnEnable_m1B592E7AC81C7F17D0A59325EBDE71E067178E8A,
	VertexJitter_OnDisable_m5567B541D1602AD54B0F437D4710BCD1951FE2C5,
	VertexJitter_Start_mB698E212B8C3B7C66C71C88F4761A4F33FCE4683,
	VertexJitter_ON_TEXT_CHANGED_m09CFD4E872042E7377BEC8B95D34F22F62ABC45B,
	VertexJitter_AnimateVertexColors_m1CCF60CA14B2FF530950D366FF078281ADC48FF4,
	VertexJitter__ctor_mC19C148659C8C97357DB56F36E14914133DA93CF,
	VertexShakeA_Awake_mD2ABEB338822E0DBCFDEC1DB46EC20BB2C44A8C2,
	VertexShakeA_OnEnable_m60947EACA10408B6EAD2EC7AC77B4E54E2666DD8,
	VertexShakeA_OnDisable_mF5AF9069E523C0DF610EF3BE2017E73A4609E3CC,
	VertexShakeA_Start_m96D17C13A279F9B442588F6448672BCF07E4A409,
	VertexShakeA_ON_TEXT_CHANGED_mE761EE13D2F9573841EFA5DEE82E545545280BFA,
	VertexShakeA_AnimateVertexColors_mC97FED540BDE59254AB30C5E6BCF1F53B766945F,
	VertexShakeA__ctor_mD84B9A0167705D5D3C8CA024D479DC7B5362E67B,
	VertexShakeB_Awake_mA59F3CA197B3A474F4D795E2B3F182179FF633CF,
	VertexShakeB_OnEnable_m8D4DD5CA81E5B0C02937D43C1782533D54F34B3F,
	VertexShakeB_OnDisable_m4473873008D4A843A26F0D2353FC36ABE74CEED2,
	VertexShakeB_Start_m24BEF670ABD1CCC864FAFE04750FEB83D916D0DF,
	VertexShakeB_ON_TEXT_CHANGED_mCC864EE9569AF68F53F6EAEB2CE8077CFEAF9E53,
	VertexShakeB_AnimateVertexColors_mAF3B23F4DD98AC3E641700B994B2994C14F0E12C,
	VertexShakeB__ctor_mDCAEC737A20F161914DD12A2FCBAB6AB7C64FEF2,
	VertexZoom_Awake_m5F98434E568929859E250743BA214C0782D17F2B,
	VertexZoom_OnEnable_m01E35B4259BA0B13EC5DA18A11535C2EC6344123,
	VertexZoom_OnDisable_mACD450919605863EC4C36DA360898BAEBBF3DDDB,
	VertexZoom_Start_m6910FF4AC88466E3B7DB867AD553429F1745320D,
	VertexZoom_ON_TEXT_CHANGED_mBFB58FE53145750AD747B38D9D1E7E00F8DBF9A0,
	VertexZoom_AnimateVertexColors_m328088EC0F893B3E60BB072F77ADF939B8566E4D,
	VertexZoom__ctor_m67833553C2739616BF059C661F92A032885510B2,
	WarpTextExample_Awake_m9643904751E2DF0A7DF536847AEA1A2B0774DD20,
	WarpTextExample_Start_mDF931C271901519BF21FD356F706FA8CDE236406,
	WarpTextExample_CopyAnimationCurve_m2C738EA265E2B35868110EE1D8FCBD4F1D61C038,
	WarpTextExample_WarpText_mAAAB1687869E0121C858C65BDB2D294D55CFB67A,
	WarpTextExample__ctor_mC3EAA1AE81FB3DA4B25F20E557EC6627A06DCB31,
	SimpleCameraController_OnEnable_mE3D6E47455F101F2DEEBC2A58D09A97CF38E80B8,
	SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47,
	SimpleCameraController_Update_mBCD24408A4A2C4053F2F98DB808BD6DE88CA998F,
	SimpleCameraController__ctor_m8DE12FC1A6C31D2D60ED78F0B574CE3F864F546E,
	Pendulum_Start_mA7335B73B384BA4338C44C2C9FDB05A03F69B5F2,
	Pendulum_Update_mDECB7850769049DBFD6AAFE677CBEB5426924853,
	Pendulum_FixedUpdate_m697D099D4FC375A5578E8CADFB2FEF62FE271346,
	Pendulum_ResetTimer_m6411AF768C9A79FB1AA0B733FB160F08FB7C9575,
	Pendulum_PendulumRotation_mECEDBA4718747F410C3CE2F2A216F0B9601A5B13,
	Pendulum__ctor_mD16958C87AFE52A859DEBECDDBE967CE28E6BF1B,
	U3CEsperar2U3Ed__4__ctor_mEEAD5D9F33D25C65EBBF914C14E6D7CF536C2421,
	U3CEsperar2U3Ed__4_System_IDisposable_Dispose_mB749B62A36613A4A052C828E3263F1C9F97DB7B7,
	U3CEsperar2U3Ed__4_MoveNext_m9B74FD9347E8FBDE5D01648CB46339432E16912E,
	U3CEsperar2U3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA7CB69F89CF3836F2385A2AF92BA4CBC5E27428,
	U3CEsperar2U3Ed__4_System_Collections_IEnumerator_Reset_mDA16E80695718B03E327E734CCA2E9F8276DD6FE,
	U3CEsperar2U3Ed__4_System_Collections_IEnumerator_get_Current_mB6CF901683B5AB6FC94119A8A9E61E6AF61BBC07,
	U3CEsperarU3Ed__5__ctor_mF98B2CA970F0CDF9F51A438C9DD1CFA78C7D7AB1,
	U3CEsperarU3Ed__5_System_IDisposable_Dispose_m48C0B099D941C0067536458198A22D50747487BB,
	U3CEsperarU3Ed__5_MoveNext_mC2BFFA96A2DB009E9B8E412E0063A4CAC961E90F,
	U3CEsperarU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA30AD4149C5B681388566CAB5518A32096A967C4,
	U3CEsperarU3Ed__5_System_Collections_IEnumerator_Reset_m5F283904469E77E19A0989FBF9213E68B3D50A16,
	U3CEsperarU3Ed__5_System_Collections_IEnumerator_get_Current_m5FC44F787B3E5E72172CBB728A08C2B9C6C2123A,
	U3CEsperarU3Ed__9__ctor_mFE96FF71D8CD21E8BC13B57D748376DE3A69A701,
	U3CEsperarU3Ed__9_System_IDisposable_Dispose_m000643BDFC09ACBACAF93905260830C990EFAFC3,
	U3CEsperarU3Ed__9_MoveNext_m35303B850631FE5382C0FB7A276DE1130BE2E485,
	U3CEsperarU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65E995C16007E8ABACEC74F6CADD0F7930C993DE,
	U3CEsperarU3Ed__9_System_Collections_IEnumerator_Reset_m0772DFCFD8BF68B10CD33C69F45A5B3D87EC4E94,
	U3CEsperarU3Ed__9_System_Collections_IEnumerator_get_Current_m1034C81EE042884DEAEAAC5D6CFF371A2D53BC3B,
	U3CEsperar2U3Ed__14__ctor_m19B1D16699B427843E612B157A58BACB8BFE9E21,
	U3CEsperar2U3Ed__14_System_IDisposable_Dispose_m0C0D8FF5276064E1192C03A10D3FC852059F602C,
	U3CEsperar2U3Ed__14_MoveNext_mAAC40F84DB540E2EA3DDC24EACF7235197388FF0,
	U3CEsperar2U3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0852DCCF04AD80877FCF0DDE635F3DB2DCCF3429,
	U3CEsperar2U3Ed__14_System_Collections_IEnumerator_Reset_m5A17D74E0F77D43C1C52EB3773958F7B7C44D2E3,
	U3CEsperar2U3Ed__14_System_Collections_IEnumerator_get_Current_m0AE458DC3830D871B550C70864B2CFCC624F8854,
	U3CEsperarU3Ed__15__ctor_m4CCEAB006DBD65D534E09B0582E4CA6FD0179526,
	U3CEsperarU3Ed__15_System_IDisposable_Dispose_m82C0C5D3752A6E9EB45AB86D4E8E34851D5F618E,
	U3CEsperarU3Ed__15_MoveNext_mC9CBBD4B30B42635C0C3E0D766957CF573139063,
	U3CEsperarU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m415825A3AD32E7530EA3D6FBE7A1CFDD9640F9FD,
	U3CEsperarU3Ed__15_System_Collections_IEnumerator_Reset_mCCF4D28828C9B7255621E727D9E1C7CBE7AF42E2,
	U3CEsperarU3Ed__15_System_Collections_IEnumerator_get_Current_mC64653195CAAEAF967B996CA7DB8459B2A5DC314,
	U3CCTPU3Ed__20__ctor_m13FF4687E630CAAD1B72AFA6FEA50BD196A87D66,
	U3CCTPU3Ed__20_System_IDisposable_Dispose_m347F3B07EE2A4E9638D2A300947A35BC8928982D,
	U3CCTPU3Ed__20_MoveNext_m5150B1278F44FD3FE347EE0214A168EC757360F9,
	U3CCTPU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2D9DC535DD170AC024562E1F0E246D6F4A28E6F,
	U3CCTPU3Ed__20_System_Collections_IEnumerator_Reset_m91EBF6C4294874C33CDF41C7C35F4E8BF7E975DD,
	U3CCTPU3Ed__20_System_Collections_IEnumerator_get_Current_m5D3F4E644161CBAEC91211343BE330082786C126,
	U3CEsperarU3Ed__21__ctor_m3CC245D7EF5D799683F69A88AB457A41B44C084F,
	U3CEsperarU3Ed__21_System_IDisposable_Dispose_mF5D320F684B4DEDCC097347244CC2B7088FB8203,
	U3CEsperarU3Ed__21_MoveNext_mEC518B9252F7B6B939E8C6A906CFC554DC478DB8,
	U3CEsperarU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B7E3C850D6509FCE77D1ACEC1C86F562E2876C2,
	U3CEsperarU3Ed__21_System_Collections_IEnumerator_Reset_m7DDB04513B42D3767954D54CA221AEE29424ACEB,
	U3CEsperarU3Ed__21_System_Collections_IEnumerator_get_Current_mEC67BE8C0D0A4AC86AB1DAF69173A8C7C838213B,
	U3CesperarU3Ed__8__ctor_m3DC3E4CB3591D026A4AACF6CF16E0DB58732E400,
	U3CesperarU3Ed__8_System_IDisposable_Dispose_m20A8262A50ACD9BF1D5161BC246932BC335B0C60,
	U3CesperarU3Ed__8_MoveNext_m340C68270030B94F266921FDEAEFE177A8868010,
	U3CesperarU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCAB13AF0233F0E6D1B81B33B43379A4BE7FCEB11,
	U3CesperarU3Ed__8_System_Collections_IEnumerator_Reset_mC34EAE423CA444E087BC0A061CFFEEC224814ADF,
	U3CesperarU3Ed__8_System_Collections_IEnumerator_get_Current_m26F482F0A7752ADA056A65DE2A76DB52FD7A31FF,
	U3CEsperarU3Ed__9__ctor_m70E77CBA65B0C3B56C691C237960570442A29C21,
	U3CEsperarU3Ed__9_System_IDisposable_Dispose_mDAAE9F2DB06DFC387D8E5BF677D617C513F31475,
	U3CEsperarU3Ed__9_MoveNext_mBC27437A9808C4BC0551363F311993BC87A25646,
	U3CEsperarU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m133094AC9AA71EA92C1A0665823BE7B90B7751C6,
	U3CEsperarU3Ed__9_System_Collections_IEnumerator_Reset_mEF1C53CB325B24E51F1C46070E50544EF0E1C042,
	U3CEsperarU3Ed__9_System_Collections_IEnumerator_get_Current_m98B814A95A6376EAB0277CD8C01D8171A273890F,
	U3CEsperar2U3Ed__10__ctor_mA915BCE09F0A20AF11455CF3182A4BE1F28C60A5,
	U3CEsperar2U3Ed__10_System_IDisposable_Dispose_m0A0FE7DCABFC6D7BF2BE21405C43D4C3AFE38536,
	U3CEsperar2U3Ed__10_MoveNext_mEE9E3C1E5CBC066709A2EE71313C67309C33A70F,
	U3CEsperar2U3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A0461B56DD631CA49C9C37825E9AE9221803D0F,
	U3CEsperar2U3Ed__10_System_Collections_IEnumerator_Reset_m8BE91E883B66BDE039193A68571A342F6909CBA4,
	U3CEsperar2U3Ed__10_System_Collections_IEnumerator_get_Current_m030BE28626C30B2EE2EFED7668B47AB883A2B195,
	U3CEsperarU3Ed__33__ctor_m8ECF8A8461A0F5A9A3B575D34E35445E039E5326,
	U3CEsperarU3Ed__33_System_IDisposable_Dispose_m440AE8058280B535E1A6CC34B6015F1AEF2F6D08,
	U3CEsperarU3Ed__33_MoveNext_mA7E6D939465C64EA1579F9FD2219C9325FC673A3,
	U3CEsperarU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A0B5ED1ED86E0A5DC88D1C88D1B954A8CCDF80D,
	U3CEsperarU3Ed__33_System_Collections_IEnumerator_Reset_mDF4D945BDDE139D70C214CA536805E5F9BB7DC88,
	U3CEsperarU3Ed__33_System_Collections_IEnumerator_get_Current_m50F97FB8F91ED8245C6E20F2BAAD7D9DADB548E4,
	U3CEsperar2U3Ed__34__ctor_m6C7D086F8D623D801383F74ACAF3722986047977,
	U3CEsperar2U3Ed__34_System_IDisposable_Dispose_mBA93BE59A4CE1DD6913E273D61623EABAA0C49FD,
	U3CEsperar2U3Ed__34_MoveNext_m394C75A6A32F8ABDBAD7F30F3DDB7A43ECE4C1D1,
	U3CEsperar2U3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD361386F8EF3803943E936C464D0DC06FA4FA2F8,
	U3CEsperar2U3Ed__34_System_Collections_IEnumerator_Reset_m30E7B3079666B5535CB44E43D8D204CD276D2A5B,
	U3CEsperar2U3Ed__34_System_Collections_IEnumerator_get_Current_m7CC662AD125622B21CA3F63BA1A4B08BDD87B23B,
	U3CRotateU3Ed__3__ctor_mAFA1AEA6E3F7EACE3C599B421A87C4B6839D7D34,
	U3CRotateU3Ed__3_System_IDisposable_Dispose_m244C4F4AB2743977DB8C2815A6930D89C41D84BA,
	U3CRotateU3Ed__3_MoveNext_m63E8E6B5A6EF29630BBD30F252CCA37319E51A6D,
	U3CRotateU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA1653C3A3E7DB369BBB6598854703607EA023D74,
	U3CRotateU3Ed__3_System_Collections_IEnumerator_Reset_m1106D07C76722C6FD541985BBFC6D5B96DE2D06C,
	U3CRotateU3Ed__3_System_Collections_IEnumerator_get_Current_m71DEDCBD4F8978966FFB254FD5D512730D4736B2,
	U3CEsperarU3Ed__4__ctor_mB084F0EA6347BB7C4A80B7BAD7B0DA822C1AC5D1,
	U3CEsperarU3Ed__4_System_IDisposable_Dispose_mC72867539A2E4A4661C2458FB45FC4DE609F041B,
	U3CEsperarU3Ed__4_MoveNext_mF574629DCFDDD2B0BA0F275E7A9E8659437DDE40,
	U3CEsperarU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB0747992314B2D66BBCAA6E9432A5771A34F640B,
	U3CEsperarU3Ed__4_System_Collections_IEnumerator_Reset_mB4E40D977B51661ADBE9D38E60E19AE8BAE8E549,
	U3CEsperarU3Ed__4_System_Collections_IEnumerator_get_Current_mBEC9D2AC34F0598E71CC24A0CAFA58E646AE371F,
	U3CStartU3Ed__4__ctor_m004DF17C34E6A9C76325BD4C65E0F328AD4B37E0,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mE7E20C5789828A8FDE263BD5ACC2D02982334B46,
	U3CStartU3Ed__4_MoveNext_mB3F55ABCABBA6717942BDC85935FDE439D09E226,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF976D094846BDC403335C76F41CAC26D53C8F1D2,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9368B3951732B0D18993DA1B889346A775F252CE,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m495592283D84E6B446F7B9B7405F250D9687DEFB,
	Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96,
	CharacterSelectionEvent__ctor_m036DA7F340B0839696EB50045AB186BD1046BE85,
	SpriteSelectionEvent__ctor_m0BC042938C4EBBB77FFAD68C1ACD74FC1C3C1052,
	WordSelectionEvent__ctor_m1C01733FD9860337084DFE63607ECE0EF8A450EA,
	LineSelectionEvent__ctor_m1C3A0C84C5C0FEA6C33FA9ED99756A85363C9EF2,
	LinkSelectionEvent__ctor_mC7034F51586C51D1DE381F6222816DC1542AFF3A,
	U3CStartU3Ed__10__ctor_m139DF863E59AD287A6C14228BB59D56E7FD2E578,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mBBFAE2F68813477259A0B665B4E81833C03C746B,
	U3CStartU3Ed__10_MoveNext_m8D27A150B4BC045DD5A1ACB2FABBB7F7F318A015,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C55687F407BA372889D6A533FB817E1EA81C165,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mE725DFFE744FAEDABF70579D04BBEB17A6CFF692,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD8EEDE3D2E1E9838472D20AE93DD750D1EE39AF8,
	U3CStartU3Ed__10__ctor_mECD4DB9695B4D04CEF08DF193DAFA21412DA40EF,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m6773C67C5C6E7E52F8C8545181173A58A6B7D939,
	U3CStartU3Ed__10_MoveNext_mB823F98B1B0907B6959EAEE4D1EBE0AA35795EA3,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF100CFC13DED969B3BBE2007B3F863DCE919D978,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mDE1D3A30ECA00E3CA2218A582F2C87EEE082E2DB,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m13710692FCBB7BB736B1F6446778124064802C83,
	U3CAnimatePropertiesU3Ed__6__ctor_mB4DA3EEEFC5ECB8376EF29EAC034162B575961B8,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mC6A68A27A902E234429E3706D6DB432BEA04A384,
	U3CAnimatePropertiesU3Ed__6_MoveNext_mDE142F6B3AAB5916834A6820A5E7B13A7B71E822,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9F4D59DDC372B977CD7297AA5F0C83D4FFBBA830,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mAC1E960F7FFCF032EE668635835954377D1469F6,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m96B6B8A0D715268DB789F3D4C60FC6DEC9E1F6E6,
	U3CWarpTextU3Ed__7__ctor_m07871FDF578BC130082658B43FB4322C15F0909E,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m3C8BC1501397E256464ADD27A32486CDE63C2BE2,
	U3CWarpTextU3Ed__7_MoveNext_m11D4701B069FCFC106319CBDCA56244EFA4C795F,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m995FCE4A3B9B270605168D01EF7439A437864C06,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m9970CA113E1A293472987C004B42771993CAA05C,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m398E9D0D09F26D5C3268EB41936ED92240474910,
	U3CStartU3Ed__4__ctor_mFB2D6F55665AAA83D38C58F58FA9DD0F5CE51351,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m72D91E3700B8E1F2053A7620793F97E01C1A2E3A,
	U3CStartU3Ed__4_MoveNext_m2839A135AFB4518430283897981AF4C91ABDBC6A,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E80ED07333F946FC221C16A77997DF51A80F87,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mAC8E620F4FCE24A409840017FB003AA1048497EF,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mD6DFD0886CCA250CB95B2828DEEEEE5EA6DC303A,
	U3CRevealCharactersU3Ed__7__ctor_mAF579198F26F3FD002CB7F4919CCB513E2B770E1,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m2B3AB12689498DE28F90CF5DA3D40AA6C31B928E,
	U3CRevealCharactersU3Ed__7_MoveNext_m16C2594A51B9D102B30646C47111F3476FFBF311,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B5325041D4E4A3F3D5575373F25A93752D9E514,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m321E1A1FD7DE7840F8433BF739D3E889FB3E9C7C,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9CF6271A74900FFA5BD4027E84D63C164C1650BC,
	U3CRevealWordsU3Ed__8__ctor_m5D2D48675C51D6CBD649C1AAD44A80CCA291F310,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m9F97B9D63E30AF41B31E44C66F31FE6B22DDFD4C,
	U3CRevealWordsU3Ed__8_MoveNext_m19EB0E15617A1893EEF1916629334CED1D8E9EA1,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m98E45A0AFB76FA46A5DA4E92E3FE114E310D8643,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m09CF6739322B1DB072CB8CFE591DBBC046008E63,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m2083C1001867012CC47AFA8158F00ED15527C603,
	U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m7E5E121E510EFDCCF0D565EBBF607F80836EBB66,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_mD0C99DF97552E843D1E86CF689372B3759134BB7,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_mA0FD89A2C5D0867977DAB0896DAB041EDFA64200,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m735F43BD7983BCC8E417573886ADC91CDED8F29B,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mBE24EA2EDAFC5FD22D633595174B122D5F84BB02,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m4199EEF69BC21DC6DDC2A0D0323AB70508CB1194,
	U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m664F9327A457FB4D53F20172479BB74A4B50675A,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_mA301D3B10770F41C50E25B8785CE78B373BBCE7C,
	U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m96E4CEDBAD5AA7AC7C96A481502B002BC711725F,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7609D75AFDAE758DAAE60202465AB3733475352B,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m6F0C933685FE89DF2DECC34EDD2FE5AD10542579,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m29F3BB8ADAADB8FD750636C535D2B50292E2DE3B,
	U3CAnimateVertexColorsU3Ed__3__ctor_mD05EA47C6D3F9DC7BE5B4F687A62244E48BC3808,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5E0C78AE94BC2C9BD7818EF0DD43D46ABF8C0172,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m8ABBF64D39EB5C5AC87AE0D833B6CBE570444347,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m328E88E5192D85CDD2157E26CD15CC0B21149AB6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_mB49B7C6E42E5B1F488ECA85B1B79AC1D6BBC3022,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m6966F60B8B8F4540B175D0C145D8A73C89CE5429,
	U3CAnimateVertexColorsU3Ed__11__ctor_m14F85BFAE5EFFAF0BBD6519F6A65B1EE36FC5F0F,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m36A05E300F1E06AE13E2BFF19C18A59F3E73424A,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m95629D41B32EBFFE935AA88081C00EB958D17F53,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m994AE653E10219CEB601B88CC22E332FF8AD1F36,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mE8D9B5795F095798688B43CF40C04C3FE3F01841,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m12F2FCDBABCD9F41374064CAD53515D2D039EFD0,
	U3CAnimateVertexColorsU3Ed__11__ctor_m93C153A3293F3E7F9AB47C712F5D8BC9CB0B179D,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m6E2E964131F208551826E353B282FBB9477CB002,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mD5F89478A4FDEA52E1261CCCF30CC88B0D693407,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6A7BC75B026CFA96FA1C123E6F7E5A5AAFC46E9,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m28679C8DDFAAD809DD115DB68829543159F47FBD,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m85E44906BDBB53959D8A47AE74B74420179AD3EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m9B592C7897687114428DF0983D5CE52A21051818,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m24A98FB24DAE578F96B90B3A6D61D18400A731B1,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m52F01BB7305705B9DE4309E1A05E2725BA06672E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB05E3BDEFD461F6516F45D404DBDEC452C646D37,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m72086FED04B8AC40B92836B894D6807AFB51BB38,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mA1E44018FA1464D953241F5FA414715C9ADE98CF,
	U3CU3Ec__DisplayClass10_0__ctor_mF154C9990B4AF8DA353F6A8C115C96FB7CB76410,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_mDEE1E28BD53D02CB2E40D0C263BBA65C0B5AC66C,
	U3CAnimateVertexColorsU3Ed__10__ctor_m92C755B17AC00199A759541B62E354765068E7F1,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mBA95580B1808E4EB047CD7F082595E1C5EE930C2,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mA307F1943028D7555032189B48F6289E09D2E220,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288CCD0BF91027A0CE3AE12D301F3E5189F0DCE,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m7C214643A52D954B2755D3477C3D0BFC85DAFF8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m666CA82A5C974B5AC09F277AFC97A82D1D1C365E,
	U3CWarpTextU3Ed__8__ctor_m63F411BEA2E513D84AAA701A1EDF0D0322FDE9C4,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m3D9631AF92186316351AFF095E5176423B84E25F,
	U3CWarpTextU3Ed__8_MoveNext_m24ED53830B643858F6068C6908E7C0E044C671A4,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07B1ABC445F7EADD1DB5F513857F1607C97AAC12,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4C7C46CC80C502CC1AD61D2F9DAF34F09226AF06,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m86693688EEF5E754D44B448E82D87FAB40D6C8B8,
	CameraState_SetFromTransform_m6467352ED87301E5F4A76456060A765CAB96AF3E,
	CameraState_Translate_m76BCC104A48EA7F125D5A50D874A2DEEA7967247,
	CameraState_LerpTowards_m883AAF2D3C7F5045B64CAF655FB84EF0FC98F282,
	CameraState_UpdateTransform_mE3349362276789C1617C01276F7DE533BBA22623,
	CameraState__ctor_m4A83DF36C7D280050EA1B101E61B7E345C31A322,
};
static const int32_t s_InvokerIndices[512] = 
{
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	1135,
	23,
	23,
	26,
	23,
	23,
	23,
	14,
	23,
	23,
	14,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	1135,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	1135,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	14,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	114,
	26,
	14,
	14,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	1677,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	26,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	26,
	23,
	1990,
	23,
	1990,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	26,
	26,
	455,
	455,
	35,
	35,
	608,
	23,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	455,
	455,
	35,
	35,
	608,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	26,
	26,
	32,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	32,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	28,
	28,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	28,
	14,
	23,
	23,
	1134,
	23,
	23,
	23,
	23,
	23,
	23,
	2494,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	56,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	26,
	1135,
	1324,
	26,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	512,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
