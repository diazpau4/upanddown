﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesactivateWater : MonoBehaviour
{
    public GameObject GameObj;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Water"))
        {
            GameObj.SetActive(false);
        }

    } 

}
