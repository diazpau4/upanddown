﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    public GameObject jugador1, jugador2, jugador3, jugador4, jugador5;
    public GameObject proyectil;
    public Camera cam;
    public bool a = true, b= false, c = false, d= false, e=false;
    
    void Start()
    {
        StartCoroutine("Esperar2");
        StartCoroutine("Esperar");
    }

    
    void Update()
    {


        if (Input.GetKeyDown(KeyCode.Y))
        {
            a = true;
            b = false;
            c = false;
            d = false;
            e = false;
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            a = false;
            b = true;
            c = false;
            d = false;
            e = false;
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            a = false;
            b = false;
            c = true;
            d = false;
            e = false;
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            a = false;
            b = false;
            c = false;
            d = true;
            e = false;

        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            a = false;
            b = false;
            c = false;
            d = false;
            e = true;
        }
    }
    IEnumerator Esperar2()
    {
        bool a = true;
        while (a)
        {
            yield return new WaitForSeconds(5f);
            Ray rayo = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, rayo.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(cam.transform.forward * 50, ForceMode.Impulse);

            Destroy(pro, 3);
        }
    }
    IEnumerator Esperar()
    {
        while (a)
        {
            yield return new WaitForSeconds(0.1f);
            transform.LookAt(jugador1.transform);
        }
        while (b)
        {
            yield return new WaitForSeconds(0.1f);
            transform.LookAt(jugador2.transform);
        }
        while (c)
        {
            yield return new WaitForSeconds(0.1f);
            transform.LookAt(jugador3.transform);

        }
        while (d)
        {
            yield return new WaitForSeconds(0.1f);
            transform.LookAt(jugador4.transform);
        }

        while (e)
        {
            yield return new WaitForSeconds(0.1f);
            transform.LookAt(jugador5.transform);
        }



    }
     
}
