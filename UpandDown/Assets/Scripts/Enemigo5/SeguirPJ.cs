﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguirPJ : MonoBehaviour
{
    
    public GameObject jugador;
    public GameObject cubo;
    public GameObject circulo;
    
    private bool si = true;
    public int rapidez;

    void Start()
    {
       
    }

    void Update()
    {
        if (si == true)
        {
            transform.LookAt(jugador.transform);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }
       
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            cubo.SetActive(false);
            circulo.SetActive(true);
            si = false;
            StartCoroutine("esperar");
        }
      
    }
    IEnumerator esperar()
    {
        yield return new WaitForSeconds(3f);
        si = true;
        cubo.SetActive(true);
        circulo.SetActive(false);
        
    }
    
    
}
