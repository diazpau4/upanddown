﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Yellow : MonoBehaviour
{
    public Color colorinicial;
    public Color colorfinal;
    public Renderer render;
    public GameObject button;
    void Start()
    {
        render = GetComponent<Renderer>();
        

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Character5 Teleport")
        {
            button.SetActive(true);
            render.material.color = colorfinal;
        }
        if (collision.gameObject.name == "Character6 Glowy")
        {
            button.SetActive(true);
            render.material.color = colorfinal;
        }

    }
}
