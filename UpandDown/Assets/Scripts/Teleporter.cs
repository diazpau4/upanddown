﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public GameObject Player;
    public GameObject TeleportTo;
    public GameObject StartTeleporter;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Teleporter"))
        {
            Player.transform.position = TeleportTo.transform.position;
        }

        if(collider.gameObject.CompareTag("SecondTeleporter"))
        {
            Player.transform.position = StartTeleporter.transform.position;
        }
    }

}
