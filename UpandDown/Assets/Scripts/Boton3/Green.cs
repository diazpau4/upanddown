﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Green : MonoBehaviour
{
    public Color colorinicial;
    public Color colorfinal;
    public Renderer render;
    public GameObject puente;
    void Start()
    {
        render = GetComponent<Renderer>();
        

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Character2 CambiaC")
        {
            puente.SetActive(true);
            render.material.color = colorfinal;
        }

    }
}
