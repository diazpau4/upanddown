﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pjgrav : MonoBehaviour
{

    Rigidbody rb;
    int cont = 0;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && cont == 0)
        {
            rb.useGravity = false;
            cont = 1;
        }
        else if (Input.GetKeyDown(KeyCode.F) && cont == 1)
        {
            rb.useGravity = true;
            rb.AddForce(Vector3.down * 50, ForceMode.Impulse);
            cont = 0;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("laser"))
        {
            Destroy(this.gameObject);
        }
    }
}
