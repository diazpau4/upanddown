﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detecta : MonoBehaviour
{
    public Explocion ex;
    public Crecimiento cr;
    public GameObject Player1,Player2,Player3,Player4,Player5;
    void Start()
    {
        ex.enabled = false;
        cr.enabled = false;
        

    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position,Player1.transform.position) < 10)
        {
            cr.enabled = true;
            StartCoroutine("Esperar");
        }
        if (Vector3.Distance(transform.position, Player1.transform.position) < 10)
        {
            cr.enabled = true;
            StartCoroutine("Esperar");
        }
        if (Vector3.Distance(transform.position, Player2.transform.position) < 10)
        {
            cr.enabled = true;
            StartCoroutine("Esperar");
        }
        if (Vector3.Distance(transform.position, Player3.transform.position) < 10)
        {
            cr.enabled = true;
            StartCoroutine("Esperar");
        }
        if (Vector3.Distance(transform.position, Player4.transform.position) < 10)
        {
            cr.enabled = true;
            StartCoroutine("Esperar");
        }
        if (Vector3.Distance(transform.position, Player5.transform.position) < 10)
        {
            cr.enabled = true;
            StartCoroutine("Esperar");
        }
    }
    IEnumerator Esperar()
    {
        yield return new WaitForSeconds(2f);
        ex.enabled = true;
    }
}
