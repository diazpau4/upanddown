﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crecimiento : MonoBehaviour
{
    public Color colorinicial;
    public Color colorfinal;
    public Renderer render;
    bool a = true;
    void Start()
    {
        render = GetComponent<Renderer>();
        
        StartCoroutine("Esperar");      
    }


    IEnumerator Esperar()
    {
        while (a)
        {
            yield return new WaitForSeconds(0.2f);
            render.material.color = colorinicial;
            yield return new WaitForSeconds(0.2f);
            render.material.color = colorfinal;
        }                          
    }
    




}
