﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotadorVert : MonoBehaviour
{
    //public float durationOfRotation = 4;
    //bool end = true;

    //void Update()
    //{
    //    if (end)
    //    {
    //        StartCoroutine(Rotate(durationOfRotation));
    //    }
    //}


    //IEnumerator Rotate(float duration)
    //{
    //    end = false;
    //    float startRotation = transform.eulerAngles.x;
    //    float endRotation = startRotation + 360.0f;
    //    float t = 0.0f;
    //    while (t < duration)
    //    {
    //        t += Time.deltaTime;
    //        float xRotation = Mathf.Lerp(startRotation, endRotation, t / duration) % 360.0f;
    //        transform.eulerAngles = new Vector3(xRotation, transform.eulerAngles.y, transform.eulerAngles.z);

    //        yield return null;
    //    }
    //    end = true;
    //}

    public int rotationSpeed = 5;


    void Update()
    {
        transform.Rotate(rotationSpeed * Time.deltaTime, 0, 0, Space.Self);
    }
}
