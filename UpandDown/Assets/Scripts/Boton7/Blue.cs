﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blue : MonoBehaviour
{
    public Color colorinicial;
    public Color colorfinal;
    public Renderer render;
    public GameObject plataforma;
    void Start()
    {
        render = GetComponent<Renderer>();


    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Character4 Suelos")
        {
            plataforma.SetActive(true);
            render.material.color = colorfinal;
        }

    }
}
