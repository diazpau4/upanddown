﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarPor : MonoBehaviour
{
    public GameObject gameObj;
    public Color colorinicial;
    public Color colorfinal;
    public Renderer render;
    
    void Start()
    {
        render = GetComponent<Renderer>();


    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Character1 RompeP")
        {
            
            render.material.color = colorfinal;
            gameObj.SetActive(true);
            
        }

    }
}
