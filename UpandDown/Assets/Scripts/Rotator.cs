﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float durationOfRotation = 4;
    bool end = true;

    // Update is called once per frame
    void Update()
    {
        if (end)
        {
            StartCoroutine(Rotate(durationOfRotation));
        }
    }


    IEnumerator Rotate(float duration)
    {
        end = false;
        float startRotation = transform.eulerAngles.y;
        float endRotation = startRotation + 360.0f;
        float t = 0.0f;
        while (t < duration)
        {
            t += Time.deltaTime;
            float yRotation = Mathf.Lerp(startRotation, endRotation, t / duration) % 360.0f;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, yRotation, transform.eulerAngles.z);

            yield return null;
        }
        end = true;
    }

}
