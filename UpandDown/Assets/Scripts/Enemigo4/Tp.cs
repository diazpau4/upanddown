﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tp : MonoBehaviour
{
    public int lugar;
    public Transform tp0,tp1,tp2,tp3;
    public GameObject bala;
    public Camera Camara;
    public GameObject jugador1, jugador2, jugador3, jugador4, jugador5;
    public bool a = true, b = false, c = false, d = false, e = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("CTP");
        StartCoroutine("Esperar");

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            a = true;
            b = false;
            c = false;
            d = false;
            e = false;
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            a = false;
            b = true;
            c = false;
            d = false;
            e = false;
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            a = false;
            b = false;
            c = true;
            d = false;
            e = false;
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            a = false;
            b = false;
            c = false;
            d = true;
            e = false;

        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            a = false;
            b = false;
            c = false;
            d = false;
            e = true;
        }

    }
    public void logica()
    {
        
        if (lugar == 0)
        {
            gameObject.transform.position = tp0.position;
        }
        if (lugar == 1)
        {
            gameObject.transform.position = tp1.position;
        }
        if (lugar == 2)
        {
            gameObject.transform.position = tp2.position;
        }
        if (lugar == 3)
        {
            gameObject.transform.position = tp3.position;
        }
        

        
       


    }
    IEnumerator CTP()
    {
        bool a = true;
        

        while (a)
        {
            yield return new WaitForSeconds(1f);
            lugar = Random.Range(0, 4);
            logica();
            Ray rayo = Camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(bala, rayo.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(Camara.transform.forward * 100, ForceMode.Impulse);

            Destroy(pro, 2);
        }
        
    }
    IEnumerator Esperar()
    {
        while (a)
        {
            yield return new WaitForSeconds(0.1f);
            transform.LookAt(jugador1.transform);
        }
        while (b)
        {
            yield return new WaitForSeconds(0.1f);
            transform.LookAt(jugador2.transform);
        }
        while (c)
        {
            yield return new WaitForSeconds(0.1f);
            transform.LookAt(jugador3.transform);

        }
        while (d)
        {
            yield return new WaitForSeconds(0.1f);
            transform.LookAt(jugador4.transform);
        }

        while (e)
        {
            yield return new WaitForSeconds(0.1f);
            transform.LookAt(jugador5.transform);
        }



    }
}
