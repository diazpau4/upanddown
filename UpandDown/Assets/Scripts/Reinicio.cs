﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reinicio : MonoBehaviour
{
    
    void Start()
    {
        
    }

   
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Character1 RompeP" || collision.gameObject.name == "Character2 CambiaC" || collision.gameObject.name == "Character3 Sticky" || collision.gameObject.name == "Character4 Suelos" || collision.gameObject.name == "Character5 Teleport")
        { 
            SceneManager.LoadScene(0);
        }
    }
}
