﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonAzul : MonoBehaviour
{
    public Color colorinicial;
    public Color colorfinal;
    public Renderer render;
    public GameObject Water;
    void Start()
    {
        render = GetComponent<Renderer>();

        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Character4 Suelos")
        {
            Water.SetActive(false);
            render.material.color = colorfinal;
        }
             
    }

}
