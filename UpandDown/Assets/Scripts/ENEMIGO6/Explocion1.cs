﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explocion1 : MonoBehaviour
{
    public int Cube = 8;
    public float delay = 1f;
    public float fuerza = 1f;
    public float radios = 2f;
    void Start()
    {
        Invoke("Main", delay);
    }
   
    // Update is called once per frame
    public void Main()
    {
        for (int x = 0; x < Cube; x++)
        {
            for (int y = 0; y < Cube; y++)
            {
                for (int z = 0; z < Cube; z++)
                {
                    crearcubo(new Vector3(x, y, z));
                }
            }
        } // creamos cubitos en el cubo
        gameObject.SetActive(false);
        //Destroy(gameObject, 3);
       
    }
    void crearcubo(Vector3 cordenadas) //creamos el mismo cubo pero modificado
    {
        GameObject cubos = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Renderer rd = cubos.GetComponent<Renderer>();
        rd.material = GetComponent<Renderer>().material;
        cubos.transform.localScale = transform.localScale / Cube; //Configuramos su escala para que sea una fraccion del cubo original
        Vector3 primercubo = transform.position - transform.localScale / 2 + cubos.transform.localScale / 2; //primer pocision del cuadrado pequeño es la primera del cubo grande menos la mitad del tamaño del cuadrado grande mas la mitad del tamaño del cuadrado pequeño
        cubos.transform.position = primercubo + Vector3.Scale(cordenadas, cubos.transform.localScale); //posicion del primer cuadrado mas la cordenada x multiplicado por su ancho mas la cordenada y multiplicada por su altura
        Rigidbody rb = cubos.AddComponent<Rigidbody>(); //le damos rigidbody a todos los cubos                   
        //rb.AddExplosionForce(fuerza, transform.position, radios); //fuerza de ezplocion, radio de la explocion y poscicion de la explocion
        cubos.gameObject.tag = "Enemigo";
    }
}
