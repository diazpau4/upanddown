﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour
{
    public Explocion1 ex;
    void Start()
    {
        ex.enabled = false;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "pjgrav" || collision.gameObject.name == "pjpare")
        {
            ex.Main();
        }
    }

}
