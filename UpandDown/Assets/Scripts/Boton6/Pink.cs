﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pink : MonoBehaviour
{
    public Color colorinicial;
    public Color colorfinal;
    public Renderer render;
    
    void Start()
    {
        render = GetComponent<Renderer>();


    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Character3 Sticky")
        {
            
            render.material.color = colorfinal;
        }

    }
}
