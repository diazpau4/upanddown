﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    
    public GameObject PjeVio;
    public GameObject PjeVer;
    public GameObject PjeRo;
    public GameObject PjeAz;
    public GameObject PjeAm;
    public GameObject PjeNar;
    public GameObject PjeMar;
    public GameObject Pjegrav;
    public GameObject Pjepare;
    public Camera cam1;
    public Camera cam2;
    public Camera cam3;
    public Camera cam4;
    public Camera cam5;
    public Camera cam6;
    public Camera cam7;
    public Camera cam8;
    public Camera cam9;
    public float mov = 10.0f;
    public float rot = 200.0f;
    public float x, y;
    private Rigidbody rb;
    public LayerMask capaPiso;
    public CapsuleCollider col;
    public GameObject enemigo2;
    public GameObject transformadores;
    public GameObject transformadores1;
    public float jumpForce = 20;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();

    }
    void Update()
    {
        
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.Rotate(0, x * Time.deltaTime * rot, 0);
        transform.Translate(0, 0, y * Time.deltaTime * mov);
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso()) 
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            


        }
 
        if (Input.GetKeyDown(KeyCode.Y))
        {
            PjeVio.gameObject.SetActive(true);
            PjeVer.gameObject.SetActive(false);
            PjeRo.gameObject.SetActive(false);
            PjeAm.gameObject.SetActive(false);
            PjeAz.gameObject.SetActive(false);
            PjeNar.gameObject.SetActive(false);
            PjeMar.gameObject.SetActive(false);
            Pjegrav.gameObject.SetActive(false);
            Pjepare.gameObject.SetActive(false);
            cam1.gameObject.SetActive(true);
            cam2.gameObject.SetActive(false);
            cam3.gameObject.SetActive(false);
            cam4.gameObject.SetActive(false);
            cam5.gameObject.SetActive(false);
            cam6.gameObject.SetActive(false);
            cam7.gameObject.SetActive(false);
            cam8.gameObject.SetActive(false);
            cam9.gameObject.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            PjeVio.gameObject.SetActive(false);
            PjeVer.gameObject.SetActive(true);
            PjeRo.gameObject.SetActive(false);
            PjeAm.gameObject.SetActive(false);
            PjeAz.gameObject.SetActive(false);
            PjeNar.gameObject.SetActive(false);
            PjeMar.gameObject.SetActive(false);
            Pjegrav.gameObject.SetActive(false);
            Pjepare.gameObject.SetActive(false);
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(true);
            cam3.gameObject.SetActive(false);
            cam4.gameObject.SetActive(false);
            cam5.gameObject.SetActive(false);
            cam6.gameObject.SetActive(false);
            cam7.gameObject.SetActive(false);
            cam8.gameObject.SetActive(false);
            cam9.gameObject.SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            PjeVio.gameObject.SetActive(false);
            PjeVer.gameObject.SetActive(false);
            PjeRo.gameObject.SetActive(true);
            PjeAm.gameObject.SetActive(false);
            PjeAz.gameObject.SetActive(false);
            PjeNar.gameObject.SetActive(false);
            PjeMar.gameObject.SetActive(false);
            Pjegrav.gameObject.SetActive(false);
            Pjepare.gameObject.SetActive(false);
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(false);
            cam3.gameObject.SetActive(true);
            cam4.gameObject.SetActive(false);
            cam5.gameObject.SetActive(false);
            cam6.gameObject.SetActive(false);
            cam7.gameObject.SetActive(false);
            cam8.gameObject.SetActive(false);
            cam9.gameObject.SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            PjeVio.gameObject.SetActive(false);
            PjeVer.gameObject.SetActive(false);
            PjeRo.gameObject.SetActive(false);
            PjeAz.gameObject.SetActive(true);
            PjeAm.gameObject.SetActive(false);
            PjeNar.gameObject.SetActive(false);
            PjeMar.gameObject.SetActive(false);
            Pjegrav.gameObject.SetActive(false);
            Pjepare.gameObject.SetActive(false);
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(false);
            cam3.gameObject.SetActive(false);
            cam4.gameObject.SetActive(true);
            cam5.gameObject.SetActive(false);
            cam6.gameObject.SetActive(false);
            cam7.gameObject.SetActive(false);
            cam8.gameObject.SetActive(false);
            cam9.gameObject.SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            PjeVio.gameObject.SetActive(false);
            PjeVer.gameObject.SetActive(false);
            PjeRo.gameObject.SetActive(false);
            PjeAz.gameObject.SetActive(false);
            PjeAm.gameObject.SetActive(true);
            PjeNar.gameObject.SetActive(false);
            PjeMar.gameObject.SetActive(false);
            Pjegrav.gameObject.SetActive(false);
            Pjepare.gameObject.SetActive(false);
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(false);
            cam3.gameObject.SetActive(false);
            cam4.gameObject.SetActive(false);
            cam5.gameObject.SetActive(true);
            cam6.gameObject.SetActive(false);
            cam7.gameObject.SetActive(false);
            cam8.gameObject.SetActive(false);
            cam9.gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            PjeVio.gameObject.SetActive(false);
            PjeVer.gameObject.SetActive(false);
            PjeRo.gameObject.SetActive(false);
            PjeAz.gameObject.SetActive(false);
            PjeAm.gameObject.SetActive(false);
            PjeNar.gameObject.SetActive(true);
            PjeMar.gameObject.SetActive(false);
            Pjegrav.gameObject.SetActive(false);
            Pjepare.gameObject.SetActive(false);
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(false);
            cam3.gameObject.SetActive(false);
            cam4.gameObject.SetActive(false);
            cam5.gameObject.SetActive(false);
            cam6.gameObject.SetActive(true);
            cam7.gameObject.SetActive(false);
            cam8.gameObject.SetActive(false);
            cam9.gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            PjeVio.gameObject.SetActive(false);
            PjeVer.gameObject.SetActive(false);
            PjeRo.gameObject.SetActive(false);
            PjeAz.gameObject.SetActive(false);
            PjeAm.gameObject.SetActive(false);
            PjeNar.gameObject.SetActive(false);
            PjeMar.gameObject.SetActive(true);
            Pjegrav.gameObject.SetActive(false);
            Pjepare.gameObject.SetActive(false);
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(false);
            cam3.gameObject.SetActive(false);
            cam4.gameObject.SetActive(false);
            cam5.gameObject.SetActive(false);
            cam6.gameObject.SetActive(false);
            cam7.gameObject.SetActive(true);
            cam8.gameObject.SetActive(false);
            cam9.gameObject.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            PjeVio.gameObject.SetActive(false);
            PjeVer.gameObject.SetActive(false);
            PjeRo.gameObject.SetActive(false);
            PjeAz.gameObject.SetActive(false);
            PjeAm.gameObject.SetActive(false);
            PjeNar.gameObject.SetActive(false);
            PjeMar.gameObject.SetActive(false);
            Pjegrav.gameObject.SetActive(true);
            Pjepare.gameObject.SetActive(false);
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(false);
            cam3.gameObject.SetActive(false);
            cam4.gameObject.SetActive(false);
            cam5.gameObject.SetActive(false);
            cam6.gameObject.SetActive(false);
            cam7.gameObject.SetActive(false);
            cam8.gameObject.SetActive(true);
            cam9.gameObject.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            PjeVio.gameObject.SetActive(false);
            PjeVer.gameObject.SetActive(false);
            PjeRo.gameObject.SetActive(false);
            PjeAz.gameObject.SetActive(false);
            PjeAm.gameObject.SetActive(false);
            PjeNar.gameObject.SetActive(false);
            PjeMar.gameObject.SetActive(false);
            Pjegrav.gameObject.SetActive(false);
            Pjepare.gameObject.SetActive(true);
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(false);
            cam3.gameObject.SetActive(false);
            cam4.gameObject.SetActive(false);
            cam5.gameObject.SetActive(false);
            cam6.gameObject.SetActive(false);
            cam7.gameObject.SetActive(false);
            cam8.gameObject.SetActive(false);
            cam9.gameObject.SetActive(true);
        }

    }

    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo2"))
        {

            StartCoroutine("Esperar2");

            StartCoroutine("Esperar");
        }
    }
    IEnumerator Esperar()
    {
        yield return new WaitForSeconds(1f);

        enemigo2.SetActive(true);
    }
    IEnumerator Esperar2()
    {
        yield return new WaitForSeconds(0.5f);
        enemigo2.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Transformadores"))
        {
            transformadores.SetActive(true);
        }
        if (other.CompareTag("Transformadores"))
        {
            transformadores1.SetActive(true);
        }
    }



}
