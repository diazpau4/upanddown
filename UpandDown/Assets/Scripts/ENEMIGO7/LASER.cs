﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LASER : MonoBehaviour
{
    //public GameObject jugador;
    public GameObject proyectil;
    public Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Esperar2");
    }

    // Update is called once per frame
    void Update()
    {
        //transform.LookAt(jugador.transform);

    }
    IEnumerator Esperar2()
    {
        bool a = true;
        while (a)
        {
            yield return new WaitForSeconds(0.5f);
            Ray rayo = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, rayo.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.useGravity = false;
            rb.AddForce(cam.transform.forward * 50, ForceMode.Impulse);

            Destroy(pro, 3);
        }
    }
}
