﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaltoSticky : MonoBehaviour
{
    public Rigidbody rb;
    public bool a = true;


    private void Start()
    {
        StartCoroutine("Esperar");
        rb = GetComponent<Rigidbody>();
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            rb.AddForce(new Vector3(10, 0, 0), ForceMode.Impulse);
        }


    }

    IEnumerator Esperar ()
    {
        
        while (a)
        {
            yield return new WaitForSeconds(0.3f);
            rb.AddForce(new Vector3(-10, 0, 0), ForceMode.Impulse);
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Boton"))
        {
            a = false;
        }
    }
}
