﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterialColor : MonoBehaviour
{
    public GameObject A, B;
    [SerializeField] private Material myMaterial;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ChangingColor"))
        {
            myMaterial.color = Color.yellow;
            A.SetActive(false);
            B.SetActive(false);
        }


        
    }
}
