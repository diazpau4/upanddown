﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverPlayer : MonoBehaviour
{
    float v = 10f;
    
    public GameObject enemigo2;
    public GameObject pared;
    public GameObject lugar;
    Rigidbody rb;
    int cont = 0;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float adelanteatras = Input.GetAxis("Horizontal") * v;
        float derechaizquierda = Input.GetAxis("Vertical") * v;
        adelanteatras *= Time.deltaTime;
        derechaizquierda *= Time.deltaTime;
        transform.Translate(adelanteatras, 0, derechaizquierda);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * 5, ForceMode.Impulse);
        }
        if (Input.GetKeyDown(KeyCode.F) && cont == 0)
        {
            rb.useGravity = false;
            cont = 1;
        }
        else if (Input.GetKeyDown(KeyCode.F) && cont == 1)
        {
            rb.useGravity = true;
            rb.AddForce(Vector3.down * 50, ForceMode.Impulse);
            cont = 0;
        }
        if (Input.GetMouseButton(0))
        {
            Instantiate(pared,lugar.transform.position,pared.transform.rotation = new Quaternion(0,90,0,0));
        }


    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemigo2"))
        {

            StartCoroutine("Esperar2");
            StartCoroutine("Esperar");
        }
    }
    IEnumerator Esperar()
    {
        yield return new WaitForSeconds(2f);

        enemigo2.SetActive(true);
    }
    IEnumerator Esperar2()
    {
        yield return new WaitForSeconds(0.5f);
        enemigo2.SetActive(false);
    }
}
