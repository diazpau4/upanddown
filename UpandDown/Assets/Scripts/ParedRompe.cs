﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParedRompe : MonoBehaviour
{
    public Explocion exp;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            exp.enabled = true;
        }
    }
}
