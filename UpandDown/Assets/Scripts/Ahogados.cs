﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ahogados : MonoBehaviour
{
    public GameObject pj;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ahogados"))
        {
            Destroy(pj);
        }
    }
    
}
