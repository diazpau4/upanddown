﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoVioleta : MonoBehaviour
{
    public Camera cam;
    public GameObject proyectil;

    void Start()
    {
        
    }

    
    void Update()
    {

        if(Input.GetMouseButtonDown (0))
        {
            Ray rayo = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, rayo.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(cam.transform.forward * 50, ForceMode.Impulse);
        }
    }
}
